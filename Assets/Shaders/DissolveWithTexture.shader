﻿Shader "Custom/Dissolve2"
{
    Properties
    {
      _MainTex ("Texture", 2D) = "white" {}
	  _Color ("Color", Color) = (1,1,1,1)
	  _Glossiness ("Smoothness", Range(0,1)) = 0.5
	  _Metallic ("Metallic", Range(0,1)) = 0.0
	  [HDR]
	  _EmissionColor ("Emission Color", Color) = (1,1,1,1)
      _DissolveTex ("Dissolve Map", 2D) = "white" {}
	  [HDR]
      _DissolveEdgeColor ("Dissolve Edge Color", Color) = (1,1,1,0)
      _DissolveIntensity ("Dissolve Intensity", Range(0.0, 1.0)) = 0
      _DissolveEdgeRange ("Dissolve Edge Range", Range(0.0, 1.0)) = 0            
      _DissolveEdgeMultiplier ("Dissolve Edge Multiplier", Float) = 1
    }
   
    SubShader
    {
		Tags { "RenderType" = "Opaque" }
		LOD 200
		Cull Off
   
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0
 
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_DissolveTex;
		};
     
		sampler2D _MainTex;
		sampler2D _DissolveTex;
		fixed4 _Color;
		fixed4 _EmissionColor;
      
		uniform float4 _DissolveEdgeColor;      
		uniform float _DissolveEdgeRange;
		uniform float _DissolveIntensity;
		uniform float _DissolveEdgeMultiplier;
		half _Glossiness;
		half _Metallic;
                
		void surf (Input IN, inout SurfaceOutputStandard o)
		{
      	
			float4 dissolveColor = tex2D(_DissolveTex, IN.uv_DissolveTex); 	      	
			half dissolveClip = dissolveColor.r - _DissolveIntensity;
			half edgeRamp = max(0, _DissolveEdgeRange - dissolveClip);
			clip( dissolveClip );
        
			float4 texColor = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			//o.Albedo = lerp( texColor, _DissolveEdgeColor, min(1, edgeRamp * _DissolveEdgeMultiplier) );
			o.Albedo = texColor;
			o.Emission = lerp( _EmissionColor, _DissolveEdgeColor, min(1, edgeRamp * _DissolveEdgeMultiplier) );
			//o.Emission = _DissolveEdgeColor;

			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = texColor.a;
		}
		ENDCG
    }
    Fallback "Diffuse"
 }