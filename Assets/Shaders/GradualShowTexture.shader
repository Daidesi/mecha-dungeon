﻿Shader "Custom/GradualShowTexture"
{
    Properties
    {
      _MainTex ("Texture", 2D) = "white" {}
	  _Color ("Color", Color) = (1,1,1,1)
	  _Glossiness ("Smoothness", Range(0,1)) = 0.5
	  _Metallic ("Metallic", Range(0,1)) = 0.0
	  [HDR]
	  _EmissionColor ("Emission Color", Color) = (1,1,1,1)
      _SecondTex ("Second Texture", 2D) = "white" {}
      _SecondTextureIntensity ("Second Texture Intensity", Range(0.0, 1.0)) = 0
    }
   
    SubShader
    {
		Tags { "RenderType" = "Opaque" }
		LOD 200
   
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0
 
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_SecondTex;
		};
     
		sampler2D _MainTex;
		sampler2D _SecondTex;
		fixed4 _Color;
		fixed4 _EmissionColor;
      
		uniform float _SecondTextureIntensity;
		half _Glossiness;
		half _Metallic;
                
		void surf (Input IN, inout SurfaceOutputStandard o)
		{
			float4 texColor = tex2D(_MainTex, IN.uv_MainTex) * _Color * (1-_SecondTextureIntensity);
			float4 tex2Color = tex2D(_SecondTex, IN.uv_SecondTex) * _Color * _SecondTextureIntensity;
			o.Albedo = texColor + tex2Color;
			o.Emission = _EmissionColor;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = texColor.a;
		}
		ENDCG
    }
    Fallback "Diffuse"
 }