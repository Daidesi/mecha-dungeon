﻿// simple "dissolving" shader by genericuser (radware.wordpress.com)
// clips materials, using an image as guidance.
// use clouds or random noise as the slice guide for best results.
  Shader "Custom/Dissolve" {
    Properties {
	  _TintColor("Tint Color", Color) = (1,1,1,1)
	  _EmissionColor("Emission Color", Color) = (1,1,1,1)
      _SliceGuide ("Slice Guide (RGB)", 2D) = "white" {}
      _SliceAmount ("Slice Amount", Range(0.0, 1.0)) = 0.5
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      Cull Off
      CGPROGRAM
      //if you're not planning on using shadows, remove "addshadow" for better performance
      #pragma surface surf Lambert addshadow
      struct Input {
          float2 uv_SliceGuide;
          float _SliceAmount;
      };
      sampler2D _SliceGuide;
      float _SliceAmount;
	  fixed4 _TintColor;
	  fixed4 _EmissionColor;
      void surf (Input IN, inout SurfaceOutput o) {
          clip(tex2D (_SliceGuide, IN.uv_SliceGuide).rgb - _SliceAmount);
          o.Albedo = _TintColor.rgb;
		  o.Emission = _EmissionColor.rgb;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }
