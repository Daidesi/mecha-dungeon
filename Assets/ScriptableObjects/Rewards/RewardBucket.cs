﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RewardBucket : ScriptableObject {
    public int basicSpawnChance = 1;
    public int bonusSpawnChance = 0;
    public GameObject[] powerUps;

    public GameObject GetRandomReward()
    {
        return powerUps[Random.Range(0, powerUps.Length)];
    }
}
