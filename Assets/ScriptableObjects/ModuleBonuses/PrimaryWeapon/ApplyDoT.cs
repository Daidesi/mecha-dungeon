﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PrimaryWeapon/ApplyDoT")]
public class ApplyDoT : PowerUp
{
    public float damageStart = 10;
    public float damageStep = 5;
    public float durationStart = 2;
    public float durationStep = 0.5f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }

    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number)
    {
        float DoTDamage = damageStart;
        DoTDamage += damageStep * (number - 1);

        float DoTDuration = durationStart;
        DoTDuration -= durationStep * (number - 1);
        if (DoTDuration < 0.5f)
        {
            DoTDuration = 0.5f;
        }

        Shot shotScript = shot.GetComponent<Shot>();
        shotScript.applyDoTEffect = true;
        shotScript.DoTdamage = DoTDamage * (playerController.damage.value + playerController.roomBonusDamage) / DoTDuration;
        shotScript.DoTduration = DoTDuration;
    }
}
