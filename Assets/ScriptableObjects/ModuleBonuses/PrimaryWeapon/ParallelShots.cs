﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PrimaryWeapon/ParallelShots")]
public class ParallelShots : PowerUp
{

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        shotCreator.numberOfParallel += 1;
    }

    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        shotCreator.numberOfParallel -= 1;
    }

    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }

    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number)
    {
        Shot shotScript = shot.GetComponent<Shot>();
        switch (number)
        {
            case 1:
                shotScript.damage *= 0.75f;
                break;
            case 2:
                shotScript.damage *= 0.6f;
                break;
            case 3:
                shotScript.damage *= 0.5f;
                break;
            default:
                throw new System.NotSupportedException("Unrecognized value.");
        }
    }
}
