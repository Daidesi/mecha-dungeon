﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PrimaryWeapon/SpreadShots")]
public class SpreadShots : PowerUp
{

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        shotCreator.numberOfSpreadPairs += 1;
        switch (number)
        {
            case 1:
                shotCreator.spreadAngle = 10;
                break;
            case 2:
                shotCreator.spreadAngle = 5;
                break;
            case 3:
                shotCreator.spreadAngle = 4;
                break;
        }
    }

    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        shotCreator.numberOfSpreadPairs -= 1;
        switch (number)
        {
            case 2:
                shotCreator.spreadAngle = 10;
                break;
            case 3:
                shotCreator.spreadAngle = 5;
                break;
            case 4:
                shotCreator.spreadAngle = 4;
                break;
        }
    }

    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }

    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number)
    {
        Shot shotScript = shot.GetComponent<Shot>();
        switch (number)
        {
            case 1:
                shotScript.damage *= 0.9f;
                break;
            case 2:
                shotScript.damage *= 0.8f;
                break;
            case 3:
                shotScript.damage *= 0.7f;
                break;
            default:
                throw new System.NotSupportedException("Unrecognized value.");
        }
    }
}
