﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PrimaryWeapon/GhostShot")]
public class GhostShot : PowerUp
{
    public Texture2D texture;
    public float chanceStart = 0.5f;
    public float chanceStep = 0.5f;
    public float basicDamage = 1.4f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }

    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number)
    {
        float chance = Random.Range(0f, 1f);
        float chanceSplit = chanceStart;
        chanceSplit += chanceStep * (number - 1);

        if (chance < chanceSplit)
        {
            Shot shotScript = shot.GetComponent<Shot>();
            shotScript.ignoreInnerObstacles = true;

            if (number > 2)
            {
                shotScript.damage *= basicDamage;
            }

            Renderer renderer = shot.GetComponent<Renderer>();
            Material mat = renderer.sharedMaterial;
            Material matNew = Instantiate(mat);
            matNew.SetTexture("_MainTex", texture);
            renderer.sharedMaterial = matNew;
        }
    }
}
