﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

[CreateAssetMenu(menuName = "SecondaryWeapon/HitEverything")]
public class HitEverything : PowerUp
{
    public float powerStart = 0.6f;
    public float powerStep = 0.4f;
    public float explosionRange = 150f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }

    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number)
    {
        float power = powerStart;
        power += powerStep * (number - 1);

        Shot shotScript = shot.GetComponent<Shot>();
        shotScript.effect = Instantiate(shotScript.effect);
        SecondaryExplosionEffect effectScript = shotScript.effect.GetComponent<SecondaryExplosionEffect>();

        effectScript.range = explosionRange;
        effectScript.damage *= power;
    }
}