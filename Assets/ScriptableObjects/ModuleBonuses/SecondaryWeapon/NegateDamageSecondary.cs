﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

[CreateAssetMenu(menuName = "SecondaryWeapon/NegateDamageSecondary")]
public class NegateDamageSecondary : PowerUp
{
    public float amountStart = 0.6f;
    public float amountStep = 0.3f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number)
    {
        if (playerController.currentSecondaryShots.value == 0)
        {
            return damage;
        }

        float neededAmount = amountStart;
        neededAmount -= amountStep * (number - 1);

        int neededShots = Mathf.FloorToInt(playerController.maxSecondaryShots.value * neededAmount);

        if ((enemy == null || enemy.tag != "Chasm") && playerController.currentSecondaryShots.value > neededShots)
        {
            playerController.currentSecondaryShots.value -= 1;
            playerController.FireSecondary();
            return 0;
        }
        
        return damage;
    }

    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
}