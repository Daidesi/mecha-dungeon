﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

[CreateAssetMenu(menuName = "SecondaryWeapon/PowerfulProc")]
public class PowerfulProc : PowerUp
{
    public GameObject powerfulExplosionEffectPrefab;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }

    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number)
    {
        float chance = Random.Range(0f, 1f);
        float chanceSplit = 0.2f;
        chanceSplit += 0.15f * (number - 1);

        if (chance < chanceSplit)
        {
            Shot shotScript = shot.GetComponent<Shot>();
            shotScript.effect = powerfulExplosionEffectPrefab;
            Renderer renderer = shot.GetComponent<Renderer>();
            
            Material mat = renderer.sharedMaterial;
            Material matNew = Instantiate(mat);
            matNew.SetColor("_TintColor", new Color(1, 0.298f, 0.298f, 0.5f));
            renderer.sharedMaterial = matNew;
        }
    }
}