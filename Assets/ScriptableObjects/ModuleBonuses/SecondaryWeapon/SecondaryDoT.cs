﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SecondaryWeapon/SecondaryDoT")]
public class SecondaryDoT : PowerUp
{
    public float damageStart = 100;
    public float damageStep = 150;
    public float durationStart = 5;
    public float durationStep = 2f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number)
    {
        float DoTDamage = damageStart;
        DoTDamage += damageStep * (number - 1);

        float DoTDuration = durationStart;
        DoTDuration += durationStep * (number - 1);

        Shot shotScript = shot.GetComponent<Shot>();
        shotScript.effect = Instantiate(shotScript.effect);
        SecondaryExplosionEffect effectScript = shotScript.effect.GetComponent<SecondaryExplosionEffect>();

        effectScript.applyDoTEffect = true;
        effectScript.DoTdamage = DoTDamage * (playerController.damage.value + playerController.roomBonusDamage) / DoTDuration;
        effectScript.DoTduration = DoTDuration;
    }

    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
