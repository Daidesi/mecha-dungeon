﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerUp: ScriptableObject{

    public string title;
    public string[] content;

    public abstract void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number);
    public abstract void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number);
    public abstract void BeforeTeleport(PlayerController playerController, Vector3 direction, int number);
    public abstract void AfterTeleport(PlayerController playerController, Vector3 direction, int number);
    public abstract void OnFirePrimary(PlayerController playerController, GameObject shot, int number);
    public abstract void OnFireSecondary(PlayerController playerController, GameObject shot, int number);
    public abstract void OnRoomEnter(PlayerController playerController, int number);
    public abstract void OnRoomCompletion(PlayerController playerController, int number);
    public abstract float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number);
    public abstract float OnHealthUp(PlayerController playerController, float healAmount, int number);
}
