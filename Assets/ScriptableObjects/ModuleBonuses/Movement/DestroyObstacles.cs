﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Movement/DestroyObstacles")]
public class DestroyObstacles : PowerUp
{
    public float lotOfDamage = 1000f;
    public float radius = 10;
    public LayerMask destroyableObstacles;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number)
    {
        RaycastHit[] hit;
        Ray ray = new Ray(playerController.transform.position, direction);

        // https://answers.unity.com/questions/282165/raycastall-returning-results-in-reverse-order-of-c-1.html
        hit = Physics.RaycastAll(ray, playerController.phaseLength + 3, destroyableObstacles).OrderBy(h => h.distance).ToArray();
        
        for (int i = 0; i < number; i++)
        {
            if (i >= hit.Length)
            {
                return;
            }

            IDealableDamage dealableDamage = hit[i].transform.GetComponent<IDealableDamage>();

            if (dealableDamage == null)
            {
                return;
            }
            dealableDamage.DealDamage(lotOfDamage, null);
        }
    }

    public override void AfterTeleport(PlayerController playerController, Vector3 position, int number)
    {
        Collider[] hit = Physics.OverlapSphere(position, radius, destroyableObstacles);
        System.Array.Sort(hit, (x, y) => (position - x.transform.position).sqrMagnitude.CompareTo((position - y.transform.position).sqrMagnitude));

        for (int i = 0; i < number; i++)
        {
            if (i >= hit.Length)
            {
                return;
            }

            IDealableDamage dealableDamage = hit[i].transform.GetComponent<IDealableDamage>();

            if (dealableDamage == null)
            {
                return;
            }
            dealableDamage.DealDamage(lotOfDamage, null);
        }
    }

    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
