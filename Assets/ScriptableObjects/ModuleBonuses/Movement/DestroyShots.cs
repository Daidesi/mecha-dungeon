﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/DestroyShots")]
public class DestroyShots : PowerUp
{
    public string tagToAffect = "PlayerComponent";
    public float radiusStart = 5;
    public float radiusStep = 3;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 position, int number)
    {
        Collider[] shots = Physics.OverlapSphere(position, radiusStart + radiusStep * (number - 1), LayerMask.GetMask("Shot"));

        foreach (var shot in shots)
        {
            Shot shotScript = shot.GetComponent<Shot>();
            if (shotScript.tagToAffect == tagToAffect)
            {
                Vector3 shotPosition = shot.transform.position;
                shot.transform.position = new Vector3(shotPosition.x, 100, shotPosition.z);
                Destroy(shot.gameObject);
            }
        }
    }

    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
