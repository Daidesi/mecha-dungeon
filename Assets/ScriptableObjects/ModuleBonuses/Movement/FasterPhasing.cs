﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/FasterPhasing")]
public class FasterPhasing : PowerUp
{

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        switch (number)
        {
            case 1:
                playerController.phaseEngineCooldown *= 0.7f;
                break;
            case 2:
                playerController.phaseEngineCooldown = (playerController.phaseEngineCooldown / 0.7f) * 0.55f;
                break;
            case 3:
                playerController.phaseEngineCooldown = (playerController.phaseEngineCooldown / 0.55f) * 0.4f;
                break;
        }
    }

    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        switch (number)
        {
            case 1:
                playerController.phaseEngineCooldown /= 0.7f;
                break;
            case 2:
                playerController.phaseEngineCooldown = (playerController.phaseEngineCooldown / 0.55f) * 0.7f;
                break;
            case 3:
                playerController.phaseEngineCooldown = (playerController.phaseEngineCooldown / 0.4f) * 0.55f;
                break;
        }
    }

    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
