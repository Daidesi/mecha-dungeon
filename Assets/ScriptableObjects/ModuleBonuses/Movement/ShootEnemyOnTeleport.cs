﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/ShootEnemyOnTeleport")]
public class ShootEnemyOnTeleport : PowerUp
{

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number)
    {
        Vector3 position = playerController.gameObject.transform.position;
        Collider[] enemies = Physics.OverlapSphere(position, 100, LayerMask.GetMask("Enemy"));

        System.Array.Sort(enemies, (x, y) => (position - x.transform.position).sqrMagnitude.CompareTo((position - y.transform.position).sqrMagnitude));

        for (int i = 0; i < number; i++)
        {
            if (i >= enemies.Length)
            {
                return;
            }
            playerController.FirePrimary(enemies[i].transform.position - position, true);
        }
    }

    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
