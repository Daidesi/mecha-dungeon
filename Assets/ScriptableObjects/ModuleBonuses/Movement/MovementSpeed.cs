﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/MovementSpeed")]
public class MovementSpeed : PowerUp
{
    public float addedSpeed = 0.5f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        playerController.speed += addedSpeed;
    }

    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number)
    {
        playerController.speed -= addedSpeed;
    }

    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
