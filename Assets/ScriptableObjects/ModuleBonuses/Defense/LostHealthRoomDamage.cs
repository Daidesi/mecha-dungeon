﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Defense/LostHealthRoomDamage")]
public class LostHealthRoomDamage : PowerUp
{
    public float damageMultiplierStart = 3f;
    public float damageMultiplierStep = 3f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number)
    {
        playerController.roomBonusDamage += damage * (damageMultiplierStart + damageMultiplierStep * (number - 1));
        return damage;
    }

    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
