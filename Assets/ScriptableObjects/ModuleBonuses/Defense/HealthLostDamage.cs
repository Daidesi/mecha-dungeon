﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Defense/HealthLostDamage")]
public class HealthLostDamage : PowerUp
{
    public float damageStart = 0.005f;
    public float damageStep = 0.005f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number) { return damage; }
    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number)
    {
        float healthLost = playerController.maxHealth.value - playerController.currentHealth.value;
        Shot shotScript = shot.GetComponent<Shot>();
        shotScript.damageMultiplier += healthLost * (damageStart + (number - 1) * damageStep);
    }

    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number)
    {
        float healthLost = playerController.maxHealth.value - playerController.currentHealth.value;
        Shot shotScript = shot.GetComponent<Shot>();
        shotScript.damageMultiplier += healthLost * (damageStart + (number - 1) * damageStep);
    }
}
