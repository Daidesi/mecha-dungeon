﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Defense/NegateDamage")]
public class NegateDamage : PowerUp
{
    public float chanceStart = 0.2f;
    public float chanceStep = 0.15f;

    public override void Up(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void Down(PlayerController playerController, ShotCreatorPlayer shotCreator, int number) { }
    public override void AfterTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override void BeforeTeleport(PlayerController playerController, Vector3 direction, int number) { }
    public override float OnDamageTaken(PlayerController playerController, float damage, GameObject enemy, int number)
    {
        float chance = Random.Range(0f, 1f);
        float chanceSplit = chanceStart;
        chanceSplit += chanceStep * (number - 1);

        if (chance < chanceSplit)
        {
            return 0;
        }

        return damage;
    }

    public override float OnHealthUp(PlayerController playerController, float healAmount, int number) { return healAmount; }
    public override void OnRoomCompletion(PlayerController playerController, int number) { }
    public override void OnRoomEnter(PlayerController playerController, int number) { }
    public override void OnFireSecondary(PlayerController playerController, GameObject shot, int number) { }
    public override void OnFirePrimary(PlayerController playerController, GameObject shot, int number) { }
}
