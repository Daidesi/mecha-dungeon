﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Level : ScriptableObject {

    [Header("Enemy spawn chance")]
    public int blueSpawnChance = 10;
    public int greenSpawnChance = 0;
    public int redSpawnChance = 0;

    [Space(10)]
    [Header("Rooms")]
    public int normalRooms = 6;
    public int challengeRooms = 1;
    public int moduleRooms = 1;
    public int floorNumber = 0;
    public GameObject chasmPrefab;
    public Material materialWallInner;
    public Material materialWallDestructible;
    public Material materialGround;
    public Material materialGroundHorizontal;
    public Material materialGroundVertical;
    public Material materialWall;
    public Material materialWallMedium;
    public Material materialWallLong;

    [Space(10)]
    [Header("Reward spawn")]
    public float rewardChanceStart = 0.2f;
    public float rewardChanceStep = 0.15f;

    [Space(10)]
    [Header("Boss room")]
    public bool isBossFloor = false;
    public int bossFloorID = 0;
}
