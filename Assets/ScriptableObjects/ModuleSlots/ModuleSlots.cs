﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ModuleSlots : ScriptableObject {
    public List<Module> value;

    public int GetNumberOfModulesWithEquipped(int slot, Module module)
    {
        int sum = 0;
        for (int i = 0; i < value.Count; i++)
        {
            if (i == slot)
            {
                sum++;
                continue;
            }
            if (value[i] == module)
            {
                sum++;
            }
        }
        return sum;
    }

    public int GetNumberOfModules(int slot)
    {
        if (value[slot] == null)
        {
            return 0;
        }
        Module module = value[slot];
        int sum = 0;
        for (int i = 0; i < value.Count; i++)
        {
            if (value[i] == module)
            {
                sum++;
            }
        }
        return sum;
    }

    public int GetNumberOfModules(Module module)
    {
        int sum = 0;
        for (int i = 0; i < value.Count; i++)
        {
            if (value[i] == module)
            {
                sum++;
            }
        }
        return sum;
    }
}
