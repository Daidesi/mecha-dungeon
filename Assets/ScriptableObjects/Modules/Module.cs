﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Module : ScriptableObject {
    public string title;
    public Sprite image;
    public PowerUp primaryWeapon;
    public PowerUp secondaryWeapon;
    public PowerUp movement;
    public PowerUp defense;
    public GameObject onDestroyPowerUp;
    public string onDestroyPowerUpDescription;
}
