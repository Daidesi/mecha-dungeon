﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMesh_agent_cursor : MonoBehaviour
{

    public Camera cam;
    public float speed;

    private UnityEngine.AI.NavMeshAgent agent;
    private Rigidbody rb;

    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        agent.updatePosition = false;
        agent.updateRotation = false;
    }

    // Update is called once per frame
    void Update()
    {
        agent.nextPosition = transform.position;
        rb.AddForce(speed * agent.desiredVelocity);
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                agent.SetDestination(hit.point);
            }
        }
    }
}
