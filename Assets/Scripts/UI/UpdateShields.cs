﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateShields : MonoBehaviour {

    public RectTransform healthbar;
    public GameObject shieldPrefab;
    public IntVariable shields;
    public RectTransform shieldBar;
    public int offset = 40;
    public int shieldOffset = 15;

    private int currentShields = 0;
    private int actualXCoord = 0;
    private List<GameObject> shieldsList;

    // Use this for initialization
    void Start () {
        shieldsList = new List<GameObject>();
        
        AddShield(shields.value);
    }

    void AddShield(int number)
    {
        for (int i = 0; i < number; i++)
        {
            GameObject gameObject = Instantiate(shieldPrefab, transform, false);
            RectTransform rectTransform = (RectTransform)gameObject.transform;
            rectTransform.anchoredPosition = new Vector2(actualXCoord, 0);
            shieldsList.Add(gameObject);

            currentShields++;
            actualXCoord += shieldOffset;
        }
    }

    void RemoveShield(int number)
    {
        for (int i = 0; i < number; i++)
        {
            GameObject gameObject = shieldsList[shieldsList.Count - 1];
            Destroy(gameObject);
            shieldsList.RemoveAt(shieldsList.Count - 1);

            currentShields--;
            actualXCoord -= shieldOffset;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (currentShields < shields.value)
        {
            AddShield(shields.value - currentShields);
        }
        if (currentShields > shields.value)
        {
            RemoveShield(currentShields - shields.value);
        }

        shieldBar.anchoredPosition = new Vector2(healthbar.sizeDelta.x + offset, shieldBar.anchoredPosition.y);
	}
}
