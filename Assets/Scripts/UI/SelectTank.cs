﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectTank : MonoBehaviour {

    public GameObject mainMenu;
    public IntVariable selectedTank;

    [Space(10)]
    [Header("Power tank button")]
    public GameObject[] powerTankObjectsWhileLocked;
    public GameObject[] powerTankObjectsWhileUnlocked;
    public Button powerTankButton;

    [Space(10)]
    [Header("Speed tank button")]
    public GameObject[] speedTankObjectsWhileLocked;
    public GameObject[] speedTankObjectsWhileUnlocked;
    public Button speedTankButton;
    
    void Update () {
		if (Input.GetButtonDown("Cancel"))
        {
            mainMenu.SetActive(true);
            gameObject.SetActive(false);
        }
	}

    public void Setup()
    {
        bool powerUnlocked = PlayerPrefs.GetInt("PowerUnlocked", 0) == 1;
        bool speedUnlocked = PlayerPrefs.GetInt("SpeedUnlocked", 0) == 1;

        // Allow interaction only if unlocked
        if (powerUnlocked)
        {
            powerTankButton.interactable = true;
            foreach (var gameObject in powerTankObjectsWhileLocked)
            {
                gameObject.SetActive(false);
            }
            foreach (var gameObject in powerTankObjectsWhileUnlocked)
            {
                gameObject.SetActive(true);
            }
        }
        else
        {
            powerTankButton.interactable = false;
            foreach (var gameObject in powerTankObjectsWhileLocked)
            {
                gameObject.SetActive(true);
            }
            foreach (var gameObject in powerTankObjectsWhileUnlocked)
            {
                gameObject.SetActive(false);
            }
        }

        if (speedUnlocked)
        {
            speedTankButton.interactable = true;
            foreach (var gameObject in speedTankObjectsWhileLocked)
            {
                gameObject.SetActive(false);
            }
            foreach (var gameObject in speedTankObjectsWhileUnlocked)
            {
                gameObject.SetActive(true);
            }
        }
        else
        {
            speedTankButton.interactable = false;
            foreach (var gameObject in speedTankObjectsWhileLocked)
            {
                gameObject.SetActive(true);
            }
            foreach (var gameObject in speedTankObjectsWhileUnlocked)
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void StartGame(int selected)
    {
        selectedTank.value = selected;
        SceneManager.LoadScene(1);
    }
}
