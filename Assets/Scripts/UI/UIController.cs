﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    public PauseScreen pauseScreen;
    public ModuleSelector moduleSelectionScreen;
    public UpdateMinimapLarge largeMinimap;
    public GameOver gameOver;
    public GameOver winScreen;
    public Texture2D crossHairTexture;

    public FloatVariable currentHealth;
    
	void Update () {
        bool pauseActive = pauseScreen.gameObject.activeSelf;
        bool moduleSelection = moduleSelectionScreen.gameObject.activeSelf;
        bool minimapShown = largeMinimap.gameObject.activeSelf;
        bool gameOverActive = gameOver.gameObject.activeSelf;
        bool winScreenActive = winScreen.gameObject.activeSelf;

        if (Input.GetButtonDown("Cancel"))
        {
            if (gameOverActive || winScreenActive)
            {
                SceneManager.LoadScene(0);
            }
            else if (!moduleSelection)
            {
                if (!pauseActive)
                {
                    ShowMouse();
                    pauseScreen.Setup();
                }
                else
                {
                    ShowCrossHair();
                    pauseScreen.ResumeGame();
                }
            }
        }
        if (Input.GetButtonDown("Minimap"))
        {

            if (!moduleSelection && !pauseActive)
            {
                if (!minimapShown)
                {
                    largeMinimap.gameObject.SetActive(true);
                    largeMinimap.Show();
                }
                else
                {
                    largeMinimap.gameObject.SetActive(false);
                }
            }
        }
        if (!gameOverActive && currentHealth.value <= 0)
        {
            gameOver.Show();
        }
    }

    public void ShowModuleScreen(Module module)
    {
        ShowMouse();
        moduleSelectionScreen.Setup(module);
    }

    public void ShowWinScreen()
    {
        winScreen.Show();
    }

    public void ShowCrossHair()
    {
        Cursor.SetCursor(crossHairTexture, new Vector2(32, 32), CursorMode.Auto);
    }

    public void ShowMouse()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}
