﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateMinimap : MonoBehaviour {

    public GameObject currentRoom;
    public GameObject visitedRoom;
    public GameObject unvisitedRoom;

    public GenerateLevel levelGenerator;
    public GameObject player;

    public Sprite challengeRoom;
    public Sprite moduleRoom;
    public Sprite transitionRoom;
    public Sprite startRoom;
    public Sprite bossRoom;

    [HideInInspector]
    public char[,] layout;
    [HideInInspector]
    public int[,] layoutVisited;
    [HideInInspector]
    public int currentX = 0;
    [HideInInspector]
    public int currentY = 0;
    
	void Start () {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        ResetMinimap();
    }
	
	void Update () {
        int x, y;
        GetCurrentRoomIndex(out x, out y);

        // Check if room changed since last minimap generation
        if (layout != null && (currentX != x || currentY != y))
        {
            currentX = x;
            currentY = y;
            // Update map of visited rooms
            if (layoutVisited[x, y] != 2)
            {
                layoutVisited[x, y] = 2;
                if (x != 0 && layout[x - 1, y] != '\0' && layoutVisited[x - 1, y] == 0)
                {
                    layoutVisited[x - 1, y] = 1;
                }
                if (x != 10 && layout[x + 1, y] != '\0' && layoutVisited[x + 1, y] == 0)
                {
                    layoutVisited[x + 1, y] = 1;
                }
                if (y != 0 && layout[x, y - 1] != '\0' && layoutVisited[x, y - 1] == 0)
                {
                    layoutVisited[x, y - 1] = 1;
                }
                if (y != 10 && layout[x, y + 1] != '\0' && layoutVisited[x, y + 1] == 0)
                {
                    layoutVisited[x, y + 1] = 1;
                }
            }
            RemoveAllNodes();

            for (int i = -2; i <= 2; i++)
            {
                for (int j = -2; j <= 2; j++)
                {
                    if (x + i < 0 || x + i > 10)
                    {
                        continue;
                    }

                    if (y + j < 0 || y + j > 10)
                    {
                        continue;
                    }

                    if (i == 0 && j == 0)
                    {
                        GameObject node = Instantiate(currentRoom, transform, false);
                        RectTransform nodeTransform = (RectTransform)node.transform;
                        nodeTransform.anchoredPosition = new Vector2(i * 25, j * 25);

                        if (layout[x + i, y + j] == 'G')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.7f);
                            image.sprite = transitionRoom;
                        }
                        if (layout[x + i, y + j] == 'T')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.7f);
                            image.sprite = moduleRoom;
                        }
                        if (layout[x + i, y + j] == 'C')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.7f);
                            image.sprite = challengeRoom;
                        }
                        if (layout[x + i, y + j] == 'S')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.7f);
                            image.sprite = startRoom;
                        }
                        if (layout[x + i, y + j] == 'B')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.7f);
                            image.sprite = bossRoom;
                        }
                    }
                    else if (layoutVisited[x + i, y + j] == 1)
                    {
                        GameObject node = Instantiate(unvisitedRoom, transform, false);
                        RectTransform nodeTransform = (RectTransform)node.transform;
                        nodeTransform.anchoredPosition = new Vector2(i * 25, j * 25);
                    }
                    else if (layoutVisited[x + i, y + j] == 2)
                    {
                        GameObject node = Instantiate(visitedRoom, transform, false);
                        RectTransform nodeTransform = (RectTransform)node.transform;
                        nodeTransform.anchoredPosition = new Vector2(i * 25, j * 25);

                        if (layout[x + i, y + j] == 'G')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.6f);
                            image.sprite = transitionRoom;
                        }
                        if (layout[x + i, y + j] == 'T')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.6f);
                            image.sprite = moduleRoom;
                        }
                        if (layout[x + i, y + j] == 'C')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.6f);
                            image.sprite = challengeRoom;
                        }
                        if (layout[x + i, y + j] == 'S')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.6f);
                            image.sprite = startRoom;
                        }
                        if (layout[x + i, y + j] == 'B')
                        {
                            Image image = node.transform.Find("Image").GetComponent<Image>();
                            image.color = new Color(1, 1, 1, 0.6f);
                            image.sprite = bossRoom;
                        }
                    }
                }
            }
        }
        else if (layout == null)
        {
            ResetMinimap();
        }
	}
    
    void GetCurrentRoomIndex(out int x, out int y)
    {
        x = ((int) (player.transform.position.x + 62.5f + 5 * 125) / 125);
        y = ((int) (player.transform.position.z + 42.5f + 5 * 85) / 85);
    }

    void RemoveAllNodes()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void ResetMinimap()
    {
        layout = levelGenerator.Layout;
        layoutVisited = new int[11, 11];
    }
}
