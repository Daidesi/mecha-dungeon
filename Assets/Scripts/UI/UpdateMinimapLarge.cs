﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateMinimapLarge : MonoBehaviour {

    public GameObject currentRoom;
    public GameObject visitedRoom;
    public GameObject unvisitedRoom;

    public Sprite challengeRoom;
    public Sprite moduleRoom;
    public Sprite transitionRoom;
    public Sprite startRoom;
    public Sprite bossRoom;

    public UpdateMinimap minimap;

    private int currentX;
    private int currentY;

    public void Update()
    {
        // Redraw large minimap on room change
        if (currentX != minimap.currentX || currentY != minimap.currentY)
        {
            currentX = minimap.currentX;
            currentY = minimap.currentY;

            Show();
        }
    }

    public void Show ()
    {
        int[,] visited = minimap.layoutVisited;
        char[,] layout = minimap.layout;
        
        RemoveAllNodes();

        for (int i = 0; i < 11; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                if (i == minimap.currentX && j == minimap.currentY)
                {
                    GameObject node = Instantiate(currentRoom, transform, false);
                    RectTransform nodeTransform = (RectTransform)node.transform;
                    nodeTransform.anchoredPosition = new Vector2((i - 5) * 25, (j - 5) * 25);

                    if (layout[i, j] == 'G')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.7f);
                        image.sprite = transitionRoom;
                    }
                    if (layout[i, j] == 'T')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.7f);
                        image.sprite = moduleRoom;
                    }
                    if (layout[i, j] == 'C')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.7f);
                        image.sprite = challengeRoom;
                    }
                    if (layout[i, j] == 'S')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.7f);
                        image.sprite = startRoom;
                    }
                    if (layout[i, j] == 'B')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.7f);
                        image.sprite = bossRoom;
                    }
                }
                else if (visited[i, j] == 1)
                {
                    GameObject node = Instantiate(unvisitedRoom, transform, false);
                    RectTransform nodeTransform = (RectTransform)node.transform;
                    nodeTransform.anchoredPosition = new Vector2((i - 5) * 25, (j - 5) * 25);
                }
                else if (visited[i, j] == 2)
                {
                    GameObject node = Instantiate(visitedRoom, transform, false);
                    RectTransform nodeTransform = (RectTransform)node.transform;
                    nodeTransform.anchoredPosition = new Vector2((i - 5) * 25, (j - 5) * 25);

                    if (layout[i, j] == 'G')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.6f);
                        image.sprite = transitionRoom;
                    }
                    if (layout[i, j] == 'T')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.6f);
                        image.sprite = moduleRoom;
                    }
                    if (layout[i, j] == 'C')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.6f);
                        image.sprite = challengeRoom;
                    }
                    if (layout[i, j] == 'S')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.6f);
                        image.sprite = startRoom;
                    }
                    if (layout[i, j] == 'B')
                    {
                        Image image = node.transform.Find("Image").GetComponent<Image>();
                        image.color = new Color(1, 1, 1, 0.6f);
                        image.sprite = bossRoom;
                    }
                }
            }
        }
    }

    void RemoveAllNodes()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
