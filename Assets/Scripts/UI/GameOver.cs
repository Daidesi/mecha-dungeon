﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOver : MonoBehaviour {

    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI continueText;
    public float gameOverShowTime = 4f;
    public float continueShowTime = 2f;
    public float continueWaitTime = 4f;

    private float gameOverTimer;
    private float continueTimer;
    
    void Update () {
        SetAlpha(gameOverText, Mathf.Clamp((Time.time - gameOverTimer) / gameOverShowTime, 0, 1));
        SetAlpha(continueText, Mathf.Clamp((Time.time - continueTimer) / continueShowTime, 0, 1));
    }

    public void Show()
    {
        SetAlpha(gameOverText, 0);
        SetAlpha(continueText, 0);
        gameOverTimer = Time.time;
        continueTimer = Time.time + continueWaitTime;

        gameObject.SetActive(true);
    }

    void SetAlpha(TextMeshProUGUI text, float alpha)
    {
        Color color = text.color;
        color.a = alpha;
        text.color = color;
    }
}
