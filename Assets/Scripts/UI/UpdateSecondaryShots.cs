﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateSecondaryShots : MonoBehaviour {

    public GameObject secondaryShotPrefab;
    public IntVariable secondaryShots;
    public IntVariable maxSecondaryShots;

    private int currentMaxShots = 0;
    private int actualXCoord = 0;
    private List<GameObject> shots;
    private List<Image> shotImages;
    
    void Start () {
        shots = new List<GameObject>();
        shotImages = new List<Image>();

        AddShot(maxSecondaryShots.value);
    }

    void AddShot(int number)
    {
        for (int i = 0; i < number; i++)
        {
            GameObject gameObject = Instantiate(secondaryShotPrefab, transform, false);
            RectTransform rectTransform = (RectTransform)gameObject.transform;
            rectTransform.anchoredPosition = new Vector2(actualXCoord, 0);

            GameObject shot = gameObject.transform.Find("Image").Find("Shot").gameObject;
            shotImages.Add(shot.GetComponent<Image>());
            shots.Add(gameObject);

            currentMaxShots++;
            actualXCoord += 15;
        }
    }

    void RemoveShot(int number)
    {
        for (int i = 0; i < number; i++)
        {
            GameObject gameObject = shots[shots.Count - 1];
            Destroy(gameObject);
            shots.RemoveAt(shots.Count - 1);
            shotImages.RemoveAt(shotImages.Count - 1);
            currentMaxShots--;
            actualXCoord -= 15;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (currentMaxShots < maxSecondaryShots.value)
        {
            AddShot(maxSecondaryShots.value - currentMaxShots);
        }
        if (currentMaxShots > maxSecondaryShots.value)
        {
            RemoveShot(currentMaxShots - maxSecondaryShots.value);
        }

        for (int i = 0; i < currentMaxShots; i++)
        {
            if (i < secondaryShots.value)
            {
                shotImages[i].fillAmount = 1;
            }
            else
            {
                shotImages[i].fillAmount = 0;
            }
        }
	}
}
