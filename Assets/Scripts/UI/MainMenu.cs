﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public GameObject newGameButton;
    public Options options;

    private void Start()
    {
        EventSystem.current.SetSelectedGameObject(newGameButton);
        options.Setup();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
