﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ModuleSelector : MonoBehaviour {
    
    public Module module;
    public Button[] primaryWeaponButtons;
    public Button[] secondaryWeaponButtons;
    public Button[] movementButtons;
    public Button[] defenseButtons;

    public ModuleSlots primaryWeaponSlots;
    public ModuleSlots secondaryWeaponSlots;
    public ModuleSlots movementSlots;
    public ModuleSlots defenseSlots;

    public TextMeshProUGUI moduleName;
    public Image moduleImage;
    public TextMeshProUGUI addedBonusText1;
    public TextMeshProUGUI addedBonusText2;
    public TextMeshProUGUI removedBonusText1;
    public TextMeshProUGUI removedBonusText2;
    public TextMeshProUGUI noEffectText1;
    public TextMeshProUGUI noEffectText2;
    public TextMeshProUGUI selectSlotText;

    public float locationYUp1;
    public float locationYUp2;
    public float locationYMiddle1;
    public float locationYMiddle2;
    public Sprite emptyModuleSlotImage;
    public Color badColor;
    public Color normalColor;

    private PlayerController playerController;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        Setup(module);
    }

    private void Update()
    {
        GameObject selected = EventSystem.current.currentSelectedGameObject;
        if (selected == null)
        {
            SetAddedBonusTextVisibility(false);
            SetRemovedBonusTextVisibility(false);
            SetNoEffectTextVisibility(false);
            SelectSlotTextVisibility(true);
        }
    }

    public void Setup(Module module)
    {
        this.module = module;

        gameObject.SetActive(true);
        PauseGame();

        moduleImage.sprite = module.image;
        moduleName.text = module.title;

        for (int i = 0; i < primaryWeaponButtons.Length; i++)
        {
            // primary weapon slots
            Image image = primaryWeaponButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= primaryWeaponSlots.value.Count)
            {
                primaryWeaponButtons[i].gameObject.SetActive(false);
            }
            else if (primaryWeaponSlots.value[i] != null)
            {
                image.sprite = primaryWeaponSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }

            // secondary weapon slots
            image = secondaryWeaponButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= secondaryWeaponSlots.value.Count)
            {
                secondaryWeaponButtons[i].gameObject.SetActive(false);
            }
            else if (secondaryWeaponSlots.value[i] != null)
            {
                image.sprite = secondaryWeaponSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }

            // movement slots
            image = movementButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= movementSlots.value.Count)
            {
                movementButtons[i].gameObject.SetActive(false);
            }
            else if (movementSlots.value[i] != null)
            {
                image.sprite = movementSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }

            // defense slots
            image = defenseButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= defenseSlots.value.Count)
            {
                defenseButtons[i].gameObject.SetActive(false);
            }
            else if (defenseSlots.value[i] != null)
            {
                image.sprite = defenseSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }
        }
    }

    public void ShowPrimaryWeaponBonus(int position)
    {
        if (primaryWeaponSlots.value[position] == null)
        {
            ShowBonusInfo(position, primaryWeaponSlots, module.primaryWeapon, null);
        }
        else
        {
            PowerUp removedPowerUp = primaryWeaponSlots.value[position].primaryWeapon;
            ShowBonusInfo(position, primaryWeaponSlots, module.primaryWeapon, removedPowerUp);
        }
    }

    public void ShowSecondaryWeaponBonus(int position)
    {
        if (secondaryWeaponSlots.value[position] == null)
        {
            ShowBonusInfo(position, secondaryWeaponSlots, module.secondaryWeapon, null);
        }
        else
        {
            PowerUp removedPowerUp = secondaryWeaponSlots.value[position].secondaryWeapon;
            ShowBonusInfo(position, secondaryWeaponSlots, module.secondaryWeapon, removedPowerUp);
        }
    }

    public void ShowMovementBonus(int position)
    {
        if (movementSlots.value[position] == null)
        {
            ShowBonusInfo(position, movementSlots, module.movement, null);
        }
        else
        {
            PowerUp removedPowerUp = movementSlots.value[position].movement;
            ShowBonusInfo(position, movementSlots, module.movement, removedPowerUp);
        }
    }

    public void ShowDefenseBonus(int position)
    {
        if (defenseSlots.value[position] == null)
        {
            ShowBonusInfo(position, defenseSlots, module.defense, null);
        }
        else
        {
            PowerUp removedPowerUp = defenseSlots.value[position].defense;
            ShowBonusInfo(position, defenseSlots, module.defense, removedPowerUp);
        }
    }

    private void ShowBonusInfo(int position, ModuleSlots slots, PowerUp addedPowerUp, PowerUp removedPowerUp)
    {
        int addedLevel = slots.GetNumberOfModulesWithEquipped(position, module);
        int removedLevel = slots.GetNumberOfModules(position);
        if (module == slots.value[position])
        {
            SetAddedBonusTextVisibility(false);
            SetRemovedBonusTextVisibility(false);
            SetNoEffectTextVisibility(true);
            SelectSlotTextVisibility(false);
            noEffectText1.text = addedPowerUp.title + " (lvl. " + addedLevel + "):";
            noEffectText2.text = addedPowerUp.content[removedLevel - 1];
            noEffectText1.color = normalColor;
        }
        else if (slots.value[position] == null)
        {
            SetAddedBonusTextVisibility(true);
            SetRemovedBonusTextVisibility(false);
            SetNoEffectTextVisibility(false);
            SelectSlotTextVisibility(false);
            addedBonusText1.text = addedPowerUp.title + " (lvl. " + addedLevel + "):";
            addedBonusText2.text = addedPowerUp.content[addedLevel - 1];

            RectTransform rectTransform1 = (RectTransform)addedBonusText1.transform;
            RectTransform rectTransform2 = (RectTransform)addedBonusText2.transform;

            rectTransform1.anchoredPosition = new Vector2(rectTransform1.anchoredPosition.x, locationYMiddle1);
            rectTransform2.anchoredPosition = new Vector2(rectTransform2.anchoredPosition.x, locationYMiddle2);
        }
        else
        {
            SetAddedBonusTextVisibility(true);
            SetRemovedBonusTextVisibility(true);
            SetNoEffectTextVisibility(false);
            SelectSlotTextVisibility(false);
            addedBonusText1.text = addedPowerUp.title + " (lvl. " + addedLevel + "):";
            addedBonusText2.text = addedPowerUp.content[addedLevel - 1];
            removedBonusText1.text = removedPowerUp.title + " (lvl. " + removedLevel + "):";
            removedBonusText2.text = removedPowerUp.content[removedLevel - 1];

            RectTransform rectTransform1 = (RectTransform)addedBonusText1.transform;
            RectTransform rectTransform2 = (RectTransform)addedBonusText2.transform;

            rectTransform1.anchoredPosition = new Vector2(rectTransform1.anchoredPosition.x, locationYUp1);
            rectTransform2.anchoredPosition = new Vector2(rectTransform2.anchoredPosition.x, locationYUp2);
        }
    }

    public void ShowDestroyModuleInfo()
    {
        SetAddedBonusTextVisibility(false);
        SetRemovedBonusTextVisibility(false);
        SetNoEffectTextVisibility(true);
        SelectSlotTextVisibility(false);
        noEffectText1.text = "Destroy module:";
        noEffectText2.text = "Use its remnants to create powerup (" + module.onDestroyPowerUpDescription + ")";
        noEffectText1.color = badColor;
    }

    private void SetAddedBonusTextVisibility(bool visibility)
    {
        addedBonusText1.gameObject.SetActive(visibility);
        addedBonusText2.gameObject.SetActive(visibility);
    }

    private void SetRemovedBonusTextVisibility(bool visibility)
    {
        removedBonusText1.gameObject.SetActive(visibility);
        removedBonusText2.gameObject.SetActive(visibility);
    }

    private void SetNoEffectTextVisibility(bool visibility)
    {
        noEffectText1.gameObject.SetActive(visibility);
        noEffectText2.gameObject.SetActive(visibility);
    }

    private void SelectSlotTextVisibility(bool visibility)
    {
        selectSlotText.gameObject.SetActive(visibility);
    }

    public void SetSelected(GameObject gameObject)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void SelectPrimaryWeaponSlot(int position)
    {
        Module equipedModule = primaryWeaponSlots.value[position];
        primaryWeaponSlots.value[position] = module;
        playerController.AddPowerUp(module.primaryWeapon);

        if (equipedModule != null)
        {
            playerController.RemovePowerUp(equipedModule.primaryWeapon);
        }
        ResumeGame();
    }

    public void SelectSecondaryWeaponSlot(int position)
    {
        Module equipedModule = secondaryWeaponSlots.value[position];
        secondaryWeaponSlots.value[position] = module;
        playerController.AddPowerUp(module.secondaryWeapon);

        if (equipedModule != null)
        {
            playerController.RemovePowerUp(equipedModule.secondaryWeapon);
        }
        ResumeGame();
    }

    public void SelectMovementSlot(int position)
    {
        Module equipedModule = movementSlots.value[position];
        movementSlots.value[position] = module;
        playerController.AddPowerUp(module.movement);

        if (equipedModule != null)
        {
            playerController.RemovePowerUp(equipedModule.movement);
        }
        ResumeGame();
    }

    public void SelectDefenseSlot(int position)
    {
        Module equipedModule = defenseSlots.value[position];
        defenseSlots.value[position] = module;
        playerController.AddPowerUp(module.defense);

        if (equipedModule != null)
        {
            playerController.RemovePowerUp(equipedModule.defense);
        }
        ResumeGame();
    }

    public void SelectDestroyModule()
    {
        Instantiate(module.onDestroyPowerUp, playerController.transform.position, Quaternion.identity);
        ResumeGame();
    }
}
