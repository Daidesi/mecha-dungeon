﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour {

    [Space(10)]
    [Header("Tank attributes and equiped modules")]
    public ModuleSlots primaryWeaponSlots;
    public ModuleSlots secondaryWeaponSlots;
    public ModuleSlots movementSlots;
    public ModuleSlots defenseSlots;
    public FloatVariable currentHealth;
    public FloatVariable maxHealth;
    public IntVariable currentShots;
    public IntVariable maxShots;
    public FloatVariable damage;
    
    [Header("Modules")]
    public Button[] primaryWeaponButtons;
    public Button[] secondaryWeaponButtons;
    public Button[] movementButtons;
    public Button[] defenseButtons;
    public TextMeshProUGUI selectedModuleName;
    public Sprite emptyModuleSlotImage;

    [Space(10)]
    [Header("Modifiers")]
    public TextMeshProUGUI[] primaryWeaponNames;
    public TextMeshProUGUI[] primaryWeaponBonuses;
    public TextMeshProUGUI[] secondaryWeaponNames;
    public TextMeshProUGUI[] secondaryWeaponBonuses;
    public TextMeshProUGUI[] movementNames;
    public TextMeshProUGUI[] movementBonuses;
    public TextMeshProUGUI[] defenseNames;
    public TextMeshProUGUI[] defenseBonuses;
    public Color bonusColor;
    public Color noBonusColor;

    [Space(10)]
    [Header("Stats")]
    public TextMeshProUGUI currentHealthText;
    public TextMeshProUGUI maxHealthText;
    public TextMeshProUGUI currentShotsText;
    public TextMeshProUGUI maxShotsText;
    public TextMeshProUGUI damageText;

    [Space(10)]
    [Header("Control buttons")]
    public Button mainMenuButton;
    public Button returnToGameButton;


    private void Update()
    {
        GameObject selected = EventSystem.current.currentSelectedGameObject;
        if (selected == mainMenuButton.gameObject || selected == returnToGameButton.gameObject || selected == null)
        {
            selectedModuleName.gameObject.SetActive(false);
        }
    }

    public void Setup()
    {
        gameObject.SetActive(true);
        PauseGame();

        // Setup modules
        for (int i = 0; i < primaryWeaponButtons.Length; i++)
        {
            // primary weapon slots
            Image image = primaryWeaponButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= primaryWeaponSlots.value.Count)
            {
                primaryWeaponButtons[i].gameObject.SetActive(false);
            }
            else if (primaryWeaponSlots.value[i] != null)
            {
                image.sprite = primaryWeaponSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }

            // secondary weapon slots
            image = secondaryWeaponButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= secondaryWeaponSlots.value.Count)
            {
                secondaryWeaponButtons[i].gameObject.SetActive(false);
            }
            else if (secondaryWeaponSlots.value[i] != null)
            {
                image.sprite = secondaryWeaponSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }

            // movement slots
            image = movementButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= movementSlots.value.Count)
            {
                movementButtons[i].gameObject.SetActive(false);
            }
            else if (movementSlots.value[i] != null)
            {
                image.sprite = movementSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }

            // defense slots
            image = defenseButtons[i].transform.GetChild(0).GetComponent<Image>();
            if (i >= defenseSlots.value.Count)
            {
                defenseButtons[i].gameObject.SetActive(false);
            }
            else if (defenseSlots.value[i] != null)
            {
                image.sprite = defenseSlots.value[i].image;
            }
            else
            {
                image.sprite = emptyModuleSlotImage;
            }
        }

        // Setup stats
        currentHealthText.text = Mathf.RoundToInt(currentHealth.value).ToString();
        maxHealthText.text = Mathf.RoundToInt(maxHealth.value).ToString();
        currentShotsText.text = currentShots.value.ToString();
        maxShotsText.text = maxShots.value.ToString();
        damageText.text = Mathf.RoundToInt(damage.value * 100).ToString() + " %";

        // Setup bonuses
        ShowPrimaryWeaponBonus();
        ShowSecondaryWeaponBonus();
        ShowMovementBonus();
        ShowDefenseBonus();
    }

    public void ShowPrimaryWeaponBonus()
    {
        List<string[]> modifiers = new List<string[]>();
        List<Module> usedModules = new List<Module>();

        foreach (var module in primaryWeaponSlots.value)
        {
            if (module == null || usedModules.IndexOf(module) != -1)
            {
                continue;
            }

            usedModules.Add(module);
            int bonusLevel = primaryWeaponSlots.GetNumberOfModules(module);

            modifiers.Add(new string[] { module.primaryWeapon.title + " (lvl. " + bonusLevel.ToString() + ")", module.primaryWeapon.content[bonusLevel - 1] });
        }

        if (modifiers.Count == 0)
        {
            foreach (var text in primaryWeaponNames)
            {
                text.gameObject.SetActive(false);
            }
            foreach (var text in primaryWeaponBonuses)
            {
                text.gameObject.SetActive(false);
            }
            primaryWeaponNames[0].gameObject.SetActive(true);
            primaryWeaponNames[0].text = "No bonuses";
            primaryWeaponNames[0].color = noBonusColor;
        }
        else
        {
            for (int i = 0; i < primaryWeaponNames.Length; i++)
            {
                if (i >= modifiers.Count)
                {
                    primaryWeaponNames[i].gameObject.SetActive(false);
                    primaryWeaponBonuses[i].gameObject.SetActive(false);
                }
                else
                {
                    primaryWeaponNames[i].gameObject.SetActive(true);
                    primaryWeaponBonuses[i].gameObject.SetActive(true);
                    primaryWeaponNames[i].text = modifiers[i][0];
                    primaryWeaponBonuses[i].text = modifiers[i][1];
                    primaryWeaponNames[i].color = bonusColor;
                }
            }
        }
    }

    public void ShowSecondaryWeaponBonus()
    {
        List<string[]> modifiers = new List<string[]>();
        List<Module> usedModules = new List<Module>();

        foreach (var module in secondaryWeaponSlots.value)
        {
            if (module == null || usedModules.IndexOf(module) != -1)
            {
                continue;
            }

            usedModules.Add(module);
            int bonusLevel = secondaryWeaponSlots.GetNumberOfModules(module);

            modifiers.Add(new string[] { module.secondaryWeapon.title + " (lvl. " + bonusLevel.ToString() + ")", module.secondaryWeapon.content[bonusLevel - 1] });
        }

        if (modifiers.Count == 0)
        {
            foreach (var text in secondaryWeaponNames)
            {
                text.gameObject.SetActive(false);
            }
            foreach (var text in secondaryWeaponBonuses)
            {
                text.gameObject.SetActive(false);
            }
            secondaryWeaponNames[0].gameObject.SetActive(true);
            secondaryWeaponNames[0].text = "No bonuses";
            secondaryWeaponNames[0].color = noBonusColor;
        }
        else
        {
            for (int i = 0; i < secondaryWeaponNames.Length; i++)
            {
                if (i >= modifiers.Count)
                {
                    secondaryWeaponNames[i].gameObject.SetActive(false);
                    secondaryWeaponBonuses[i].gameObject.SetActive(false);
                }
                else
                {
                    secondaryWeaponNames[i].gameObject.SetActive(true);
                    secondaryWeaponBonuses[i].gameObject.SetActive(true);
                    secondaryWeaponNames[i].text = modifiers[i][0];
                    secondaryWeaponBonuses[i].text = modifiers[i][1];
                    secondaryWeaponNames[i].color = bonusColor;
                }
            }
        }
    }

    public void ShowMovementBonus()
    {
        List<string[]> modifiers = new List<string[]>();
        List<Module> usedModules = new List<Module>();

        foreach (var module in movementSlots.value)
        {
            if (module == null || usedModules.IndexOf(module) != -1)
            {
                continue;
            }

            usedModules.Add(module);
            int bonusLevel = movementSlots.GetNumberOfModules(module);

            modifiers.Add(new string[] { module.movement.title + " (lvl. " + bonusLevel.ToString() + ")", module.movement.content[bonusLevel - 1] });
        }

        if (modifiers.Count == 0)
        {
            foreach (var text in movementNames)
            {
                text.gameObject.SetActive(false);
            }
            foreach (var text in movementBonuses)
            {
                text.gameObject.SetActive(false);
            }
            movementNames[0].gameObject.SetActive(true);
            movementNames[0].text = "No bonuses";
            movementNames[0].color = noBonusColor;
        }
        else
        {
            for (int i = 0; i < movementNames.Length; i++)
            {
                if (i >= modifiers.Count)
                {
                    movementNames[i].gameObject.SetActive(false);
                    movementBonuses[i].gameObject.SetActive(false);
                }
                else
                {
                    movementNames[i].gameObject.SetActive(true);
                    movementBonuses[i].gameObject.SetActive(true);
                    movementNames[i].text = modifiers[i][0];
                    movementBonuses[i].text = modifiers[i][1];
                    movementNames[i].color = bonusColor;
                }
            }
        }
    }

    public void ShowDefenseBonus()
    {
        List<string[]> modifiers = new List<string[]>();
        List<Module> usedModules = new List<Module>();

        foreach (var module in defenseSlots.value)
        {
            if (module == null || usedModules.IndexOf(module) != -1)
            {
                continue;
            }

            usedModules.Add(module);
            int bonusLevel = defenseSlots.GetNumberOfModules(module);

            modifiers.Add(new string[] { module.defense.title + " (lvl. " + bonusLevel.ToString() + ")", module.defense.content[bonusLevel - 1] });
        }

        if (modifiers.Count == 0)
        {
            foreach (var text in defenseNames)
            {
                text.gameObject.SetActive(false);
            }
            foreach (var text in defenseBonuses)
            {
                text.gameObject.SetActive(false);
            }
            defenseNames[0].gameObject.SetActive(true);
            defenseNames[0].text = "No bonuses";
            defenseNames[0].color = noBonusColor;
        }
        else
        {
            for (int i = 0; i < defenseNames.Length; i++)
            {
                if (i >= modifiers.Count)
                {
                    defenseNames[i].gameObject.SetActive(false);
                    defenseBonuses[i].gameObject.SetActive(false);
                }
                else
                {
                    defenseNames[i].gameObject.SetActive(true);
                    defenseBonuses[i].gameObject.SetActive(true);
                    defenseNames[i].text = modifiers[i][0];
                    defenseBonuses[i].text = modifiers[i][1];
                    defenseNames[i].color = bonusColor;
                }
            }
        }
    }

    public void SetSelected(GameObject gameObject)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ShowPrimaryWeaponModuleName(int position)
    {
        Module module = primaryWeaponSlots.value[position];
        if (module == null)
        {
            selectedModuleName.gameObject.SetActive(false);
        }
        else
        {
            selectedModuleName.gameObject.SetActive(true);
            selectedModuleName.text = module.title;
        }
    }

    public void ShowSecondaryWeaponModuleName(int position)
    {
        Module module = secondaryWeaponSlots.value[position];
        if (module == null)
        {
            selectedModuleName.gameObject.SetActive(false);
        }
        else
        {
            selectedModuleName.gameObject.SetActive(true);
            selectedModuleName.text = module.title;
        }
    }

    public void ShowMovementModuleName(int position)
    {
        Module module = movementSlots.value[position];
        if (module == null)
        {
            selectedModuleName.gameObject.SetActive(false);
        }
        else
        {
            selectedModuleName.gameObject.SetActive(true);
            selectedModuleName.text = module.title;
        }
    }

    public void ShowDefenseModuleName(int position)
    {
        Module module = defenseSlots.value[position];
        if (module == null)
        {
            selectedModuleName.gameObject.SetActive(false);
        }
        else
        {
            selectedModuleName.gameObject.SetActive(true);
            selectedModuleName.text = module.title;
        }
    }
}
