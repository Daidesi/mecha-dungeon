﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHealthBar : MonoBehaviour {

    public FloatVariable health;
    public FloatVariable maxHealth;
    public Image healthBar;

    public RectTransform panel;
    public RectTransform healthBar1;
    public RectTransform healthBar2;
    
    void Update () {
        healthBar.fillAmount = Mathf.Clamp(health.value / maxHealth.value, 0, 1);

        panel.sizeDelta = new Vector2(Mathf.Clamp(maxHealth.value * 2, 100, 400), 25);
        healthBar1.sizeDelta = new Vector2(Mathf.Clamp(maxHealth.value * 2, 100, 400), 25);
        healthBar2.sizeDelta = new Vector2(Mathf.Clamp(maxHealth.value * 2, 100, 400), 25);
    }
}
