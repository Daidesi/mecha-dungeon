﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class Options : MonoBehaviour {

    public GameObject mainMenu;
    public AudioMixer audioMixer;
    public TMP_Dropdown resolutionDropdown;
    public GameObject[] resetConfirm;
    public Slider volumeSlider;
    public Toggle fullScreenToggle;
    public TMP_Dropdown graphicsDropdown;

    private List<Resolution> resolutions;

    // Update is called once per frame
    void Update () {
		if (Input.GetButtonDown("Cancel"))
        {
            mainMenu.SetActive(true);
            gameObject.SetActive(false);
        }
	}

    public void Setup()
    {
        // Resolutions
        resolutions = new List<Resolution>(Screen.resolutions);
        resolutions.Reverse();
        resolutionDropdown.ClearOptions();

        List<string> resolutionStrings = new List<string>();
        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Count; i++)
        {
            resolutionStrings.Add(resolutions[i].width + " x " + resolutions[i].height);
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(resolutionStrings);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        
        // Other settings
        fullScreenToggle.isOn = Screen.fullScreen;
        float volume = PlayerPrefs.GetFloat("volume", 1);
        volumeSlider.value = volume;
        graphicsDropdown.value = QualitySettings.GetQualityLevel();

        foreach (GameObject gameObject in resetConfirm)
        {
            gameObject.SetActive(false);
        }
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("volume", volume);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }

    public void ResetProgress()
    {
        PlayerPrefs.DeleteAll();
    }
}
