﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookOnTargetInSight : MonoBehaviour
{

    public Transform target;
    public GameObject body;
    public LayerMask ignoredLayers;
    public LayerMask obstacleLayers;
    public float rotation_speed = 3f;

    private Rigidbody _rb;
    private Quaternion final_rotation;

    private void Start()
    {
        _rb = body.GetComponent<Rigidbody>();
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        int mask = ~ignoredLayers;
        Vector3 direction;

        if (Physics.Linecast(transform.position, target.position, out hit, mask, QueryTriggerInteraction.Ignore))
        {
            // check if hit object is obstacle
            if (obstacleLayers == (obstacleLayers | (1 << hit.transform.gameObject.layer)))
            {
                direction = _rb.velocity;
            }
            else
            {
                direction = target.position - transform.position;
            }
        }
        else
        {
            direction = target.position - transform.position;
        }

        float rotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        final_rotation = Quaternion.Euler(-90, rotation + 90, 180);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, final_rotation, rotation_speed);
    }
}
