﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    [Range(30, 500)]
    public float rotation_speed = 200f;
    public int x = 0;
    public int y = 0;
    public int z = 1;
    
	void Update () {
        transform.Rotate(new Vector3(x * rotation_speed, y * rotation_speed, z * rotation_speed) * Time.deltaTime);
    }
}
