﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPosition : MonoBehaviour {

    public Transform followed_object;
    
	void LateUpdate () {
        transform.position = followed_object.position;
	}
}
