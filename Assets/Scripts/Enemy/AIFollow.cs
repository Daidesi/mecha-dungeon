﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
public class AIFollow : MonoBehaviour
{

    public Transform target;
    public float speed;

    private NavMeshAgent agent;
    private Rigidbody rb;

    void Start()
    {
        agent = GetComponentInChildren<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        // disable navmesh agent movement
        agent.updatePosition = false;
        agent.updateRotation = false;

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    
    void FixedUpdate()
    {
        agent.nextPosition = transform.position;
        rb.AddForce(speed * agent.desiredVelocity);
        agent.SetDestination(target.position);
    }
}
