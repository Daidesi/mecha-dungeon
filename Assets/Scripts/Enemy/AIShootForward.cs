﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShotCreator))]
public class AIShootForward : MonoBehaviour {

    public float shotCooldown;

    private float shotTimer;
    private ShotCreator shotCreator;

    private void Start()
    {
        shotCreator = GetComponent<ShotCreator>();
    }
    
    void FixedUpdate()
    {
        if ((Time.time - shotTimer) > shotCooldown)
        {
            shotTimer = Time.time;
            shotCreator.Fire();
        }
    }
}
