﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ShotCreator))]
public class AIShootIfInSight : MonoBehaviour
{

    public Transform target;
    public float shotCooldown;
    public LayerMask ignoredLayers;
    public LayerMask obstacleLayers;
    
    private float shotTimer;
    private ShotCreator shotCreator;

    private void Start()
    {
        shotCreator = GetComponent<ShotCreator>();

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    
    void FixedUpdate()
    {
        LayerMask mask = ~ignoredLayers;
        RaycastHit hit;

        if (Physics.Linecast(transform.position, target.position, out hit, mask, QueryTriggerInteraction.Ignore))
        {
            // check if hit object is obstacle
            if (!(obstacleLayers == (obstacleLayers | (1 << hit.transform.gameObject.layer))))
            {
                if ((Time.time - shotTimer) > shotCooldown)
                {
                    shotTimer = Time.time;
                    shotCreator.Fire(shooter: transform.parent.gameObject);
                }
            }
        }
        else
        {
            if ((Time.time - shotTimer) > shotCooldown)
            {
                shotTimer = Time.time;
                shotCreator.Fire(shooter: transform.parent.gameObject);
            }
        }
    }
}
