﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour, IDealableDamage, IAppliableDoT
{
    public float health = 100;
    public GameObject spawnEffectAnimation;
    public GameObject destroyedPrefab;
    public GameObject destroyEffectAnimation;

    private float DoTTimer;
    private bool DoTActive = false;
    private float DoTDamage = 0;

    public void DealDamage(float damage, GameObject source)
    {
        health -= damage;
        if (health <= 0)
        {
            Kill();
        }
    }

    private void Kill()
    {
        // Replace current object with broken version
        gameObject.SetActive(false);
        GameObject destroyed = Instantiate(destroyedPrefab, transform.position, transform.rotation, transform.parent);

        Transform torusDestroyed = destroyed.transform.Find("Torus");
        Transform torus = transform.Find("Torus");
        if (torusDestroyed != null && torus != null)
        {
            torusDestroyed.position = torus.position;
            torusDestroyed.rotation = torus.rotation;
        }

        Transform cubeDestroyed = destroyed.transform.Find("Cube");
        Transform cube = transform.Find("Cube");
        if (cube == null)
        {
            cube = torus.Find("Cube");
        }

        if (cubeDestroyed != null && cube != null)
        {
            cubeDestroyed.position = cube.position;
            cubeDestroyed.rotation = cube.rotation;
        }

        GameObject effect = Instantiate(destroyEffectAnimation, transform.position, Quaternion.identity);
        Destroy(effect, 3f);

    }

    private void FixedUpdate()
    {
        // Apply damage from damage over time effect
        if (DoTActive)
        {
            DealDamage(Time.fixedDeltaTime * DoTDamage, null);
            if (DoTTimer < Time.time)
            {
                DoTActive = false;
            }
        }
    }

    public void ApplyDoTEffect(float damagePerSec, float duration)
    {
        DoTDamage = damagePerSec;
        DoTTimer = Time.time + duration;
        DoTActive = true;
    }
}
