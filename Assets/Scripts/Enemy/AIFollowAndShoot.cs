﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ShotCreator))]
[RequireComponent(typeof(Rigidbody))]
public class AIFollowAndShoot : MonoBehaviour
{

    public Transform target;
    public float speed;
    public float shotCooldown;
    public LayerMask ignoredLayers;
    public LayerMask obstacleLayers;

    private NavMeshAgent agent;
    private Rigidbody rb;
    private float shotTimer;
    private ShotCreator shotCreator;

    // Use this for initialization
    void Start()
    {
        agent = GetComponentInChildren<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        // disable navmesh agent movement
        agent.updatePosition = false;
        agent.updateRotation = false;

        shotTimer = Time.time;
        shotCreator = GetComponent<ShotCreator>();

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        LayerMask mask = ~ignoredLayers;
        RaycastHit hit;
        if (!agent.isStopped)
        {
            agent.nextPosition = transform.position;
            agent.SetDestination(target.position);
            rb.AddForce(speed * agent.desiredVelocity, ForceMode.Acceleration);
        }
        if (Physics.Linecast(transform.position, target.position, out hit, mask, QueryTriggerInteraction.Ignore))
        {
            if (obstacleLayers == (obstacleLayers | (1 << hit.transform.gameObject.layer)))
            {
                agent.isStopped = false;
            }
            else
            {
                agent.isStopped = true;
                if ((Time.time - shotTimer) > shotCooldown)
                {
                    shotTimer = Time.time;
                    shotCreator.Fire(shooter: gameObject);
                }
            }
        }
        else
        {
            agent.isStopped = true;
            if ((Time.time - shotTimer) > shotCooldown)
            {
                shotTimer = Time.time;
                shotCreator.Fire(shooter: gameObject);
            }
        }
    }
}
