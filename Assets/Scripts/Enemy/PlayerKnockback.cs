﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKnockback : MonoBehaviour
{

    public float applied_force = 5;
    public float self_force = 5;
    public float damage = 15;

    public Rigidbody rb;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            PlayerController playerController = other.GetComponentInParent<PlayerController>();
            Vector3 direction = other.transform.position - transform.position;

            // Knockback and damage is applied through player controller
            playerController.ApplyKnockback(direction * applied_force, -direction * self_force, rb);
            playerController.DealDamage(damage, transform.parent.gameObject);
        }
    }
}
