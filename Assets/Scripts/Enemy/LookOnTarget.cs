﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookOnTarget : MonoBehaviour
{
    public Transform target;
    public float rotationSpeed = 3;
    private Quaternion final_rotation;

    private void Start()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    
    void Update()
    {
        Vector3 direction = target.position - transform.position;

        float rotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        final_rotation = Quaternion.Euler(-90, rotation + 90, 180);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, final_rotation, rotationSpeed);
    }
}
