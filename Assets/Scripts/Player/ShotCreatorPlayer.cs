﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCreatorPlayer : ShotCreator {


    [Space(10)]
    [Header("Secondary shot properties (if needed)")]
    public GameObject secondaryShotPrefab;

    public GameObject FireSecondary(Vector3 customDirection = default(Vector3), bool useCustomDirection = false, float damageMultiplier = 1.0f)
    {
        Quaternion direction;
        if (useCustomDirection)
        {
            direction = Quaternion.LookRotation(customDirection, Vector3.up);
        }
        else
        {
            direction = Quaternion.LookRotation(rotationPoint.right, Vector3.up);
        }
        var shot = Instantiate(secondaryShotPrefab, spawnPoint.position, direction);

        Shot shotScript = shot.GetComponent<Shot>();
        shotScript.damageMultiplier = damageMultiplier;

        return shot;
    }
}
