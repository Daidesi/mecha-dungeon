﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ShotCreator))]
public class PlayerController : MonoBehaviour, IDealableDamage
{
    [Header("Player stats")]
    public float maxHealthStart = 100f;
    public FloatVariable maxHealth;
    public FloatVariable currentHealth;
    public float damageStart = 1f;
    public FloatVariable damage;
    public int maxSecondaryShotsStart = 3;
    public IntVariable maxSecondaryShots;
    public IntVariable currentSecondaryShots;
    public int shieldsStart = 0;
    public IntVariable shields;
    public float invulnerabilityLength = 0.2f;
    public float iFramesBlinkPeriod = 0.05f;

    [Space(10)]
    [Header("Initial powerups")]
    public PowerUp[] initialPowerUps;

    [Space(10)]
    [Header("Movement")]
    public ModuleSlots primaryWeaponSlots;
    public ModuleSlots secondaryWeaponSlots;
    public ModuleSlots movementSlots;
    public ModuleSlots defenseSlots;
    public int primaryWeaponSlotsStart = 2;
    public int secondaryWeaponSlotsStart = 2;
    public int movementSlotsStart = 2;
    public int defenseSlotsStart = 2;

    [Space(10)]
    [Header("Movement")]
    public float speed = 2;
    public Transform wheels;
    public float distanceToGround = 1f;

    // Teleport out of chasm
    private int maxLength = 100;
    private Queue<Vector3> positions = new Queue<Vector3>(101);
    private float storeWaitTime = 0.2f;
    private float positionTimer = 0f;

    [Space(10)]
    [Header("Phase engine")]
    public float phaseEngineCooldown = 2f;
    public float phaseLength = 10f;
    public float distanceFromObstacle = 2.5f;
    public LayerMask obstacleLayers;
    public GameObject phaseInEffect;
    public GameObject phaseOutEffect;
    public GameObject phaseRefreshEffect;
    private float phaseEngineTimer = 0f;
    private bool isRefreshed = true;

    [Space(10)]
    [Header("Primary and secondary weapon")]
    public float primary_shot_cooldown;
    public float secondary_shot_cooldown;
    public GameObject secondaryShotFiringEffect;
    public float shotDestroyingEffectRange = 50f;
    private float primary_timer = 0f;
    private float secondary_timer = 0f;

    [Space(10)]
    [Header("Player destroyed")]
    public GameObject playerDestroyedPrefab;
    public GameObject playerDeathEffectPrefab;

    private Rigidbody _rb;
    private ShotCreatorPlayer _shotCreator;
    private Renderer[] _renderers;
    private List<PowerUp> powerUps;
    private List<int> powerUpsCount;
    [HideInInspector]
    public float roomBonusDamage = 0f;
    private bool iFrames = false;
    private float iFramesTimer = 0;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _shotCreator = GetComponent<ShotCreatorPlayer>();
        _renderers = GetComponentsInChildren<Renderer>();

        primaryWeaponSlots.value = new List<Module>();
        secondaryWeaponSlots.value = new List<Module>();
        movementSlots.value = new List<Module>();
        defenseSlots.value = new List<Module>();

        for (int i = 0; i < primaryWeaponSlotsStart; i++)
        {
            primaryWeaponSlots.value.Add(null);
        }
        for (int i = 0; i < secondaryWeaponSlotsStart; i++)
        {
            secondaryWeaponSlots.value.Add(null);
        }
        for (int i = 0; i < movementSlotsStart; i++)
        {
            movementSlots.value.Add(null);
        }
        for (int i = 0; i < defenseSlotsStart; i++)
        {
            defenseSlots.value.Add(null);
        }

        maxHealth.value = maxHealthStart;
        currentHealth.value = maxHealthStart;
        maxSecondaryShots.value = maxSecondaryShotsStart;
        currentSecondaryShots.value = maxSecondaryShotsStart;
        damage.value = damageStart;
        shields.value = shieldsStart;

        powerUps = new List<PowerUp>();
        powerUpsCount = new List<int>();

        foreach (PowerUp powerUp in initialPowerUps)
        {
            AddPowerUp(powerUp);
        }
    }

    public void AddPowerUp(PowerUp powerUp)
    {
        int index = powerUps.IndexOf(powerUp);
        if (index == -1)
        {
            powerUps.Add(powerUp);
            powerUp.Up(this, _shotCreator, 1);
            powerUpsCount.Add(1);
        }
        else
        {
            int count = ++powerUpsCount[index];
            powerUp.Up(this, _shotCreator, count);
        }
    }

    public void RemovePowerUp(PowerUp powerUp)
    {
        int index = powerUps.IndexOf(powerUp);
        if (index == -1)
        {
            throw new System.NotSupportedException("No such powerup to remove.");
        }
        else
        {
            int count = powerUpsCount[index];
            if (count == 1)
            {
                powerUp.Down(this, _shotCreator, 1);
                powerUps.RemoveAt(index);
                powerUpsCount.RemoveAt(index);
            }
            else
            {
                powerUpsCount[index] -= 1;
                powerUp.Down(this, _shotCreator, count);
            }
        }
    }

    public void FirePrimary(Vector3 direction = default(Vector3), bool useCustomDirection = false)
    {
        GameObject[] shots = _shotCreator.Fire(direction, useCustomDirection, damage.value + roomBonusDamage);

        // Primary weapon powerup callback
        foreach (GameObject shot in shots)
        {
            for (int i = 0; i < powerUps.Count; i++)
            {
                PowerUp powerUp = powerUps[i];
                int powerUpCount = powerUpsCount[i];
                powerUp.OnFirePrimary(this, shot, powerUpCount);
            }
        }
    }

    public void FireSecondary(Vector3 direction = default(Vector3), bool useCustomDirection = false)
    {
        GameObject shot = _shotCreator.FireSecondary(direction, useCustomDirection, damage.value + roomBonusDamage);
        Instantiate(secondaryShotFiringEffect, transform.position, Quaternion.identity);
        DestroyEnemyShots(shotDestroyingEffectRange);

        // Secondary weapon powerup callback
        for (int i = 0; i < powerUps.Count; i++)
        {
            PowerUp powerUp = powerUps[i];
            int powerUpCount = powerUpsCount[i];
            powerUp.OnFireSecondary(this, shot, powerUpCount);
        }
    }

    private void DestroyEnemyShots(float range)
    {
        var cols = Physics.OverlapSphere(transform.position, range);

        foreach (var col in cols)
        {
            if (col.tag == "Shot")
            {
                var shot = col.gameObject.GetComponent<Shot>();
                if (shot.tagToAffect == "PlayerComponent")
                {
                    Destroy(col.gameObject);
                }
            }
        }
    }

    public void OnRoomEnter()
    {
        // Room with enemies enter powerup callback
        for (int i = 0; i < powerUps.Count; i++)
        {
            PowerUp powerUp = powerUps[i];
            int powerUpCount = powerUpsCount[i];
            powerUp.OnRoomEnter(this, powerUpCount);
        }
    }

    public void OnRoomCompletion()
    {
        // Room with enemies completion powerup callback
        for (int i = 0; i < powerUps.Count; i++)
        {
            PowerUp powerUp = powerUps[i];
            int powerUpCount = powerUpsCount[i];
            powerUp.OnRoomCompletion(this, powerUpCount);
        }
    }

    public void ResetRoomBonuses()
    {
        roomBonusDamage = 0;
    }

    public void DealDamage(float damageAmount, GameObject enemy)
    {
        if (!iFrames)
        {
            // Damage taken powerup callback
            for (int i = 0; i < powerUps.Count; i++)
            {
                PowerUp powerUp = powerUps[i];
                int powerUpCount = powerUpsCount[i];
                damageAmount = powerUp.OnDamageTaken(this, damageAmount, enemy, powerUpCount);
            }

            if (damageAmount != 0 && shields.value < 1)
            {
                currentHealth.value -= damageAmount;
            }
            else if (damageAmount != 0)
            {
                shields.value--;
            }

            if (currentHealth.value <= 0)
            {
                Kill();
            }

            if (damageAmount > 0)
            {
                iFrames = true;
                iFramesTimer = Time.time + invulnerabilityLength;
            }
        }
    }

    private void Kill()
    {
        gameObject.SetActive(false);
        GameObject destroyed = Instantiate(playerDestroyedPrefab, transform.position, transform.rotation);

        Transform towerDestroyed = destroyed.transform.Find("Tower");
        Transform tower = transform.Find("Tower");
        if (towerDestroyed != null && tower != null)
        {
            towerDestroyed.position = tower.position;
            towerDestroyed.rotation = tower.rotation;
        }

        GameObject effect = Instantiate(playerDeathEffectPrefab, transform.position, Quaternion.identity);
        Destroy(effect, 6f);
    }

    public void Heal(float healAmount)
    {
        // Heal powerup callback
        for (int i = 0; i < powerUps.Count; i++)
        {
            PowerUp powerUp = powerUps[i];
            int powerUpCount = powerUpsCount[i];
            healAmount = powerUp.OnHealthUp(this, healAmount, powerUpCount);
        }

        currentHealth.value = Mathf.Clamp(currentHealth.value + healAmount, -1, maxHealth.value);
    }

    public void RestoreShots(int amount)
    {
        currentSecondaryShots.value = Mathf.Clamp(currentSecondaryShots.value + amount, 0, maxSecondaryShots.value);
    }

    public void ApplyKnockback(Vector3 force, Vector3 back_force, Rigidbody rigidbody)
    {
        _rb.AddForce(force);
        if (rigidbody != null)
        {
            rigidbody.AddForce(back_force);
        }
    }

    private float ProportionalWheelsOnGround()
    {
        int count = 0;
        int countOnGround = 0;
        foreach (Transform wheel in wheels)
        {
            count++;
            if (Physics.Raycast(wheel.position, -Vector3.up, distanceToGround))
            {
                countOnGround++;
            }
        }
        Debug.Log(countOnGround + " / " + count);
        return countOnGround / count;
    }

    private bool AnyWheelsOnGround()
    {
        LayerMask mask = LayerMask.GetMask("Corridor", "Ground");
        foreach (Transform wheel in wheels)
        {
            Ray ray = new Ray(wheel.position, -Vector3.up);
            if (Physics.Raycast(ray, distanceToGround, mask))
            {
                return true;
            }
        }
        return false;
    }

    private bool AllWheelsOnGround()
    {
        LayerMask mask = LayerMask.GetMask("Corridor", "Ground");
        foreach (Transform wheel in wheels)
        {
            Ray ray = new Ray(wheel.position, -Vector3.up);
            if (!Physics.Raycast(ray, distanceToGround, mask))
            {
                return false;
            }
        }
        return true;
    }

    public void Teleport()
    {
        LayerMask mask = LayerMask.GetMask("Enemy");

        var positions_array = positions.ToArray();

        bool teleported = false;
        for (int i = positions_array.Length - 1; i >= 0; i--)
        {
            var position = positions_array[i];
            Collider[] hitColliders = Physics.OverlapSphere(position, 5, mask);
            if (hitColliders.Length == 0)
            {
                transform.position = position;
                teleported = true;
                break;
            }
        }
        if (!teleported)
        {
            transform.position = positions.Peek();
        }
    }

    public void Store(Vector3 position)
    {
        positions.Enqueue(position);
        if (positions.Count > maxLength)
        {
            positions.Dequeue();
        }
    }

    private void Phase(Vector3 direction)
    {
        // Before teleport powerup callback
        for (int i = 0; i < powerUps.Count; i++)
        {
            PowerUp powerUp = powerUps[i];
            int powerUpCount = powerUpsCount[i];
            powerUp.BeforeTeleport(this, direction, powerUpCount);
        }

        Vector3 afterPhasePosition = transform.position + direction.normalized * phaseLength;
        RaycastHit hit;
        Ray ray = new Ray(transform.position, direction);
        if (Physics.Raycast(ray, out hit, phaseLength + 3, obstacleLayers))
        {
            afterPhasePosition = hit.point - direction.normalized * distanceFromObstacle;
        }

        // After teleport powerup callback
        for (int i = 0; i < powerUps.Count; i++)
        {
            PowerUp powerUp = powerUps[i];
            int powerUpCount = powerUpsCount[i];
            powerUp.AfterTeleport(this, afterPhasePosition, powerUpCount);
        }

        Instantiate(phaseInEffect, transform.position, Quaternion.identity);
        transform.position = afterPhasePosition;
        Instantiate(phaseOutEffect, transform.position, Quaternion.identity, transform);
    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            if (Time.time > primary_timer)
            {
                primary_timer = Time.time + primary_shot_cooldown;
                FirePrimary();
            }
        }
        
        if (Input.GetMouseButton(1))
        {
            if (Time.time > secondary_timer)
            {
                if (currentSecondaryShots.value > 0)
                {
                    currentSecondaryShots.value -= 1;
                    secondary_timer = Time.time + secondary_shot_cooldown;
                    FireSecondary();
                }
            }
        }

        var anyWheels = AnyWheelsOnGround();
        var allWheels = AllWheelsOnGround();
        
        if (allWheels && positionTimer < Time.time)
        {
            Store(transform.position);
            positionTimer = Time.time + storeWaitTime;
        }

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        if (!isRefreshed && phaseEngineTimer < Time.time)
        {
            Instantiate(phaseRefreshEffect, transform.position, Quaternion.identity, transform);
            isRefreshed = true;
        }

        if (Input.GetButton("Jump") && movement.magnitude != 0 && phaseEngineTimer < Time.time)
        {
            Phase(movement);
            isRefreshed = false;
            phaseEngineTimer = Time.time + phaseEngineCooldown;
        }

        float wheelModifier;
        if (anyWheels)
        {
            wheelModifier = 1f;
        }
        else
        {
            wheelModifier = 0.3f;
        }

        _rb.AddForce(movement * speed * wheelModifier, ForceMode.Impulse);
    }

    private void Update()
    {
        if (iFrames)
        {
            if (iFramesTimer < Time.time)
            {
                iFrames = false;
                foreach (var _renderer in _renderers)
                {
                    _renderer.enabled = true;
                }
            }
            else
            {
                if ((int)(Time.time / iFramesBlinkPeriod) % 2 == 0)
                {
                    foreach (var _renderer in _renderers)
                    {
                        _renderer.enabled = false;
                    }
                }
                else
                {
                    foreach (var _renderer in _renderers)
                    {
                        _renderer.enabled = true;
                    }
                }
            }
        }
    }
}
