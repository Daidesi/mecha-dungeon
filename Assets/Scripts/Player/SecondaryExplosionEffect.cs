﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

// Based on ExplosionPhysicsForce script from Unity Standard Assets 

public class SecondaryExplosionEffect : DamagingEffect
{
    public float explosionForce = 20;
    public float range = 10;
    public float damage = 50;
    public LayerMask ignoredLayers;
    
    public bool applyDoTEffect = false;
    public float DoTdamage = 0;
    public float DoTduration = 0;

    private IEnumerator Start()
    {
        float multiplier = GetComponent<ParticleSystemMultiplier>().multiplier;
        float r = range * multiplier;
        var cols = Physics.OverlapSphere(transform.position, r, ~ignoredLayers);

        // Deal damage to all enemies
        foreach (var col in cols)
        {

            IDealableDamage dealableDamage = col.GetComponent<IDealableDamage>();
            if (dealableDamage == null)
            {
                Transform parent = col.transform.parent;
                if (parent != null)
                {
                    dealableDamage = col.transform.parent.GetComponent<IDealableDamage>();
                }
            }

            if (dealableDamage != null)
            {
                dealableDamage.DealDamage(damage * damageMultiplier, null);
            }

            if (applyDoTEffect)
            {
                IAppliableDoT appliableDoT = col.GetComponent<IAppliableDoT>();
                if (appliableDoT == null)
                {
                    Transform parent = col.transform.parent;
                    if (parent != null)
                    {
                        appliableDoT = col.transform.parent.GetComponent<IAppliableDoT>();
                    }
                }

                if (appliableDoT != null)
                {
                    appliableDoT.ApplyDoTEffect(DoTdamage, DoTduration);
                }
            }
        }

        // wait one frame because some explosions instantiate debris which should then
        // be pushed by physics force
        yield return null;

        cols = Physics.OverlapSphere(transform.position, r);

        var rigidbodies = new List<Rigidbody>();
        foreach (var col in cols)
        {
            if (col.tag != "Shot")
            {
                if (col.attachedRigidbody != null && !rigidbodies.Contains(col.attachedRigidbody))
                {
                    rigidbodies.Add(col.attachedRigidbody);
                }
            }
        }
        foreach (var rb in rigidbodies)
        {
            rb.AddExplosionForce(explosionForce * multiplier, transform.position, r, 0, ForceMode.Impulse);
        }
    }
}
