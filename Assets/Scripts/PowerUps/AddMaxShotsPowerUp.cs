﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMaxShotsPowerUp : BasicPowerUp
{
    public int addedMaxShots = 1;
    public IntVariable maxShots;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            if (!appliedBonus)
            {
                appliedBonus = true;
                PlayerController playerController = other.GetComponentInParent<PlayerController>();
                maxShots.value += addedMaxShots;
                playerController.RestoreShots(addedMaxShots);
                GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Destroy(gameObject);
            }
        }
    }
}
