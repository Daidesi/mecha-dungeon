﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseEffectModule : MonoBehaviour {

    public float pulseFrequency = 1f;
    public float minIntensity = 1.5f;
    public float maxIntensity = 3f;

    public GameObject[] pulsingObjects;
    public float minSize = 0.8f;
    public float maxSize = 1.2f;

    public GameObject border;
    public GameObject[] innerObjects;

    private Renderer rendererBorder;
    private List<Renderer> renderersInner;
    private Color colorBorder;
    private List<Color> colorsInner;
    private List<Vector3> scales;

	// Use this for initialization
	void Start ()
    {
        scales = new List<Vector3>();
        renderersInner = new List<Renderer>();
        colorsInner = new List<Color>();

        rendererBorder = border.GetComponent<Renderer>();
        Material material = Instantiate(rendererBorder.sharedMaterial);
        rendererBorder.sharedMaterial = material;
        colorBorder = material.color;

        foreach (var innerObject in innerObjects)
        {
            renderersInner.Add(innerObject.GetComponent<Renderer>());
            material = Instantiate(renderersInner[renderersInner.Count - 1].sharedMaterial);
            renderersInner[renderersInner.Count - 1].sharedMaterial = material;
            colorsInner.Add(material.color);
        }

        foreach (var gameObject in pulsingObjects)
        {
            scales.Add(gameObject.transform.localScale);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        float intensity = Mathf.Sin(Time.time * pulseFrequency) * (maxIntensity - minIntensity) + minIntensity + 1;
        float size = (Mathf.Sin(Time.time * pulseFrequency) + 1) / 2 * (maxSize - minSize) + minSize;
        rendererBorder.sharedMaterial.SetColor("_EmissionColor", colorBorder * intensity);
        for (int i = 0; i < renderersInner.Count; i++)
        {
            renderersInner[i].sharedMaterial.SetColor("_EmissionColor", colorsInner[i] * intensity);
        }
        for (int i = 0; i < scales.Count; i++)
        {
            pulsingObjects[i].transform.localScale = scales[i] * size;
        }
    }
}
