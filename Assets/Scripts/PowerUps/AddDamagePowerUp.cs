﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddDamagePowerUp : BasicPowerUp
{
    public float addedDamage = 0.05f;
    public FloatVariable damage;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            if (!appliedBonus)
            {
                appliedBonus = true;
                damage.value += addedDamage;
                GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Destroy(gameObject);
            }
        }
    }
}
