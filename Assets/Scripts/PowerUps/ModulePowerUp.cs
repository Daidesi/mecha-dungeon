﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModulePowerUp : BasicPowerUp
{
    public Module module;
    public UIController uIController;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!appliedBonus && other.tag == "PlayerComponent")
        {
            appliedBonus = true;
            GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 3);
            uIController.ShowModuleScreen(module);
            Destroy(gameObject);
        }
    }
}
