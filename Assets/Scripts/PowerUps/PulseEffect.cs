﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseEffect : MonoBehaviour {

    public float pulseFrequency = 1f;
    public float minIntensity = 1.5f;
    public float maxIntensity = 3f;

    public GameObject[] pulsingObjects;
    public float minSize = 0.8f;
    public float maxSize = 1.2f;
    
    private Renderer[] renderers;
    private Color color;
    private List<Vector3> scales;

	// Use this for initialization
	void Start ()
    {
        scales = new List<Vector3>();
        renderers = GetComponentsInChildren<Renderer>();
        Material material = Instantiate(renderers[0].sharedMaterial);
        color = material.color;
        foreach (var renderer in renderers)
        {
            renderer.sharedMaterial = material;
        }

        foreach (var gameObject in pulsingObjects)
        {
            scales.Add(gameObject.transform.localScale);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        float intensity = Mathf.Sin(Time.time * pulseFrequency) * (maxIntensity - minIntensity) + minIntensity + 1;
        float size = (Mathf.Sin(Time.time * pulseFrequency) + 1) / 2 * (maxSize - minSize) + minSize;
        foreach (var renderer in renderers)
        {
            renderer.sharedMaterial.SetColor("_EmissionColor", color * intensity);
        }
        for (int i = 0; i < scales.Count; i++)
        {
            pulsingObjects[i].transform.localScale = scales[i] * size;
        }
    }
}
