﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddShieldsPowerUp : BasicPowerUp
{
    public int addedShields = 1;
    public IntVariable shields;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            if (!appliedBonus)
            {
                appliedBonus = true;
                shields.value += addedShields;
                GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Destroy(gameObject);
            }
        }
    }
}
