﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreHealthPowerUp : BasicPowerUp
{
    public float healedAmount = 0.2f;
    public FloatVariable health;
    public FloatVariable maxHealth;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            if (!appliedBonus && health.value < maxHealth.value)
            {
                appliedBonus = true;
                PlayerController playerController = other.GetComponentInParent<PlayerController>();
                playerController.Heal(maxHealth.value * healedAmount);
                GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Destroy(gameObject);
            }
        }
    }
}
