﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreShotsPowerUp : BasicPowerUp
{

    public float restoredAmount = 0.5f;
    public IntVariable shots;
    public IntVariable maxShots;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            if (!appliedBonus && shots.value < maxShots.value)
            {
                appliedBonus = true;
                PlayerController playerController = other.GetComponentInParent<PlayerController>();
                playerController.RestoreShots(Mathf.FloorToInt(maxShots.value * restoredAmount));
                GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Destroy(gameObject);
            }
        }
    }
}
