﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMaxHealthPowerUp : BasicPowerUp
{
    public float addedMaxHealth = 10;
    public FloatVariable maxHealth;

    private bool appliedBonus = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            if (!appliedBonus)
            {
                appliedBonus = true;
                PlayerController playerController = other.GetComponentInParent<PlayerController>();
                maxHealth.value += addedMaxHealth;
                playerController.Heal(addedMaxHealth);
                GameObject effect = Instantiate(powerUpTakenEffectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 3);
                Destroy(gameObject);
            }
        }
    }
}
