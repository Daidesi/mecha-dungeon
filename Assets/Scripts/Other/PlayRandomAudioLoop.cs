﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomAudioLoop : MonoBehaviour {

    public AudioClip[] audioClips;

    private AudioSource _audio;
    
	void Start () {
        _audio = GetComponent<AudioSource>();

        if (!_audio.playOnAwake)
        {
            AudioClip randomClip = audioClips[Random.Range(0, audioClips.Length)];
            _audio.clip = randomClip;
            _audio.Play();
        }
	}
	
	void Update () {
        if (!_audio.isPlaying)
        {
            AudioClip randomClip = audioClips[Random.Range(0, audioClips.Length)];
            _audio.clip = randomClip;
            _audio.Play();
        }
    }
}
