﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTexture : MonoBehaviour {

    public Texture2D[] frames;
    public float framesPerSecond = 10f;

    private Renderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        int index = (int) Mathf.Floor(Time.time * framesPerSecond);
        index %= frames.Length;
        _renderer.sharedMaterial.mainTexture = frames[index];
    }
}
