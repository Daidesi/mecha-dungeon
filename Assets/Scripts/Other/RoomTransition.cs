﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTransition : MonoBehaviour {

    public Vector3 playerMoveVector;
    public Vector3 cameraMoveVector;

    private Camera mainCamera;
    private float shortCooldown = 0.1f;
    private float cooldownTimer = 0;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (cooldownTimer < Time.time && other.tag == "PlayerComponent")
        {
            cooldownTimer = Time.time + shortCooldown;
            var player = other.transform.parent.transform.parent;
            player.transform.position += playerMoveVector;

            mainCamera.transform.position += cameraMoveVector;
            PlayerController playerController = player.GetComponentInParent<PlayerController>();
            playerController.Store(player.transform.position);
            playerController.ResetRoomBonuses();
        }
    }
}
