﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot: MonoBehaviour
{
    public GameObject shooter;
    public float shotSpeed;
    public float knockback;
    public float damage;
    public float damageMultiplier = 1f;
    public GameObject effect;
    public string tagToAffect;
    public bool applyDamageMultiplierToEffect = false;
    public bool ignoreInnerObstacles = false;
    public bool applyDoTEffect = false;
    public float DoTdamage = 0;
    public float DoTduration = 0;

    private Rigidbody _rb;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.velocity = transform.forward * shotSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == tagToAffect)
        {
            Rigidbody rb = other.attachedRigidbody;
            rb.AddForce(transform.forward * knockback);
            Destroy(gameObject);
            GameObject shotEffect = Instantiate(effect, transform.position, Quaternion.LookRotation(-transform.forward));

            if (applyDamageMultiplierToEffect)
            {
                DamagingEffect damagingEffect = shotEffect.GetComponent<DamagingEffect>();
                damagingEffect.damageMultiplier = damageMultiplier;
            }

            IDealableDamage dealableDamage = other.GetComponent<IDealableDamage>();
            if (dealableDamage == null)
            {
                dealableDamage = other.GetComponentInParent<IDealableDamage>();
            }
            if (dealableDamage != null)
            {
                dealableDamage.DealDamage(damage * damageMultiplier, shooter);
            }

            if (applyDoTEffect)
            {
                IAppliableDoT appliableDoT = other.GetComponent<IAppliableDoT>();

                if (appliableDoT != null)
                {
                    appliableDoT.ApplyDoTEffect(DoTdamage, DoTduration);
                }
            }

        }
        if (!ignoreInnerObstacles && (other.tag == "WallInner"))
        {
            Destroy(gameObject);
            Instantiate(effect, transform.position, Quaternion.LookRotation(-transform.forward));
        }
        if (other.tag == "Destructible")
        {
            Destroy(gameObject);
            Instantiate(effect, transform.position, Quaternion.LookRotation(-transform.forward));
            IDealableDamage dealableDamage = other.GetComponent<IDealableDamage>();
            if (dealableDamage != null)
            {
                dealableDamage.DealDamage(damage * damageMultiplier, shooter);
            }
        }

        if (other.tag == "Wall")
        {
            Destroy(gameObject);
            Instantiate(effect, transform.position, Quaternion.LookRotation(-transform.forward));
        }
    }
}
