﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dissolve : MonoBehaviour {

    public float timeToDissolve = 1f;

    private Renderer[] _rend;
    private Light _light;
    private float defaultLightRange;
    private float timer;
    
    void Start () {
        timer = Time.time;

        // Get components for rendering
        _light = GetComponentInChildren<Light>();
        defaultLightRange = _light.range;
        _rend = GetComponentsInChildren<Renderer>();
        foreach (var renderer in _rend)
        {
            Material mat = renderer.sharedMaterial;
            renderer.sharedMaterial = Instantiate(mat);
        }
    }
	
	void Update () {
        // Calculate correct values for light dim and object dissolve effects
        float dissolveIntensity = (Time.time - timer) / timeToDissolve;
        float lightRange = defaultLightRange * (1 - dissolveIntensity);

        foreach (var renderer in _rend)
        {
            renderer.sharedMaterial.SetFloat("_DissolveIntensity", dissolveIntensity);
        }
        _light.range = lightRange;

        if (Time.time > timer + timeToDissolve)
        {
            gameObject.SetActive(false);
        }
    }
}
