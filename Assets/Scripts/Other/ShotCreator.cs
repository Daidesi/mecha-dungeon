﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCreator : MonoBehaviour {

    [Header("Basic Shot position and rotation")]
    public GameObject shotPrefab;
    public Transform spawnPoint;
    public Transform rotationPoint;
    public float shotSpeed = -1;
    public float knockback = -1;
    public float damage = -1;
    [Space(10)]

    [Header("Number of shots and properties")]
    public int numberOfParallel = 1;
    public float parallelShotOffset = 0.5f;
    public int numberOfSpreadPairs = 0;
    public float spreadAngle = 20f;

    public GameObject[] Fire(Vector3 customDirection = default(Vector3), bool useCustomDirection = false, float damageMultiplier = 1.0f, GameObject shooter = null)
    {
        Quaternion direction;
        if (useCustomDirection)
        {
            direction = Quaternion.LookRotation(customDirection, Vector3.up);
        }
        else
        {
            direction = Quaternion.LookRotation(rotationPoint.right, Vector3.up);
        }
        List<GameObject> shots = new List<GameObject>();
        
        for (var i = 0; i < numberOfParallel; i++)
        {
            var offset = (-(numberOfParallel - 1) * parallelShotOffset/2) + i * parallelShotOffset;
            var shot = Instantiate(shotPrefab, spawnPoint.position + (direction * Vector3.right) * offset, direction);
            SetupShot(shot, damageMultiplier, shooter);
            shots.Add(shot);
        }

        for (var i = 0; i < numberOfSpreadPairs; i++)
        {
            var offset = parallelShotOffset/2 * (i + 1);
            var angleOffset = spreadAngle * (i + 1);
            var shot = Instantiate(shotPrefab, spawnPoint.position + (direction * Vector3.right) * offset, Quaternion.LookRotation(Quaternion.AngleAxis(angleOffset, Vector3.up) * rotationPoint.right, rotationPoint.forward));
            SetupShot(shot, damageMultiplier, shooter);
            shots.Add(shot);
            shot = Instantiate(shotPrefab, spawnPoint.position - (direction * Vector3.right) * offset, Quaternion.LookRotation(Quaternion.AngleAxis(-angleOffset, Vector3.up) * rotationPoint.right, rotationPoint.forward));
            SetupShot(shot, damageMultiplier, shooter);
            shots.Add(shot);
        }

        return shots.ToArray();
    }

    public void SetupShot(GameObject shot, float damageMultiplier, GameObject shooter)
    {
        var shotScript = shot.GetComponent<Shot>();

        if (shotSpeed != -1)
        {
            shotScript.shotSpeed = shotSpeed;
        }

        if (knockback != -1)
        {
            shotScript.knockback = knockback;
        }

        if (damage != -1)
        {
            shotScript.damage = damage;
        }
        shotScript.damageMultiplier = damageMultiplier;
        shotScript.shooter = shooter;
    }
}
