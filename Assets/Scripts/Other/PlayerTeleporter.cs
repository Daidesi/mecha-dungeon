﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleporter : MonoBehaviour
{
    public float fallDamage = 10f;
    public float fallDamageCooldown = 0.05f;

    private float fallTimer = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerComponent")
        {
            PlayerController playerController = other.gameObject.GetComponentInParent<PlayerController>();

            if (playerController == null)
            {
                playerController = other.GetComponentInParent<PlayerController>();
            }
            
            if (fallTimer < Time.time)
            {
                playerController.DealDamage(fallDamage, gameObject);
                fallTimer = Time.time + fallDamageCooldown;
            }
            playerController.Teleport();
        }
    }
}
