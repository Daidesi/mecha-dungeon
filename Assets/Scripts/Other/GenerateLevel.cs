﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
using SimpleJSON;
using System.IO;

public class GenerateLevel : MonoBehaviour {

    public NavMeshSurface surface;
    public float playerTransferDistance = 30f;
    public IntVariable selectedTank;
    public GameObject[] tanksPrefab;

    [Space(10)]
    [Header("Room building prefabs")]
    public GameObject groundPrefab;
    public GameObject wallPrefab;
    public GameObject wallLongPrefab;
    public GameObject wallMediumPrefab;
    public GameObject wallInnerPrefab;
    public GameObject destructibleWallPrefab;
    public GameObject corridorPrefab;
    public GameObject roomControllerPrefab;
    public GameObject roomTransitionPrefab;
    public GameObject nextLevelPrefab;
    public GameObject navmeshObstacle;
    public GameObject transitionLightChallengePrefab;
    public GameObject transitionLightModulePrefab;
    public GameObject transitionLightNextLevelPrefab;

    [Space(10)]
    [Header("Enemies")]
    public GameObject[] towerPrefab;
    public GameObject[] chaserPrefab;
    public GameObject[] shooterPrefab;
    public GameObject[] trapShooterPrefab;

    [Space(10)]
    [Header("Rewards")]
    public RewardBucket restorativeSmallPowerUps;
    public RewardBucket restorativeBigPowerUps;
    public RewardBucket passiveSmallPowerUps;
    public RewardBucket passiveBigPowerUps;
    public GameObject[] modules;

    [Space(10)]
    [Header("Reward spawn")]
    public FloatVariable spawnChanceStep;
    public FloatVariable spawnChance;
    public FloatVariable spawnChanceStart;

    [Space(10)]
    [Header("Levels")]
    public Level[] levels;

    [Space(10)]
    [Header("Other")]
    public UpdateMinimap minimapScript;
    public UIController uIController;
    public ModuleSlots[] moduleSlotsAll;

    private JSONNode rooms;
    private Level currentLevel;
    private int index = 0;
    private Transform roomsTransform;
    private GameObject player;

    public char[,] Layout { get; private set; }

    private void Awake()
    {
        Time.timeScale = 1;
        Instantiate(tanksPrefab[selectedTank.value], new Vector3(0, 3.5f, 0), Quaternion.identity);
    }
    
    void Start ()
    {
        // Replace cursor
        uIController.ShowCrossHair();

        // Create game object for holding rooms
        GameObject roomsGameObject = new GameObject("Rooms");
        roomsTransform = roomsGameObject.transform;

        rooms = LoadLevelsFromJSON();
        player = GameObject.FindGameObjectWithTag("Player");

        // Initialize floor and spawn chance
        GenerateNextFloor();
        spawnChance.value = spawnChanceStart.value;
    }

    public void GenerateNextFloor()
    {
        if (index == levels.Length)
        {
            // No more levels => Game won
            uIController.ShowWinScreen();
            PlayerPrefs.SetInt("PowerUnlocked", 1);
            
            bool noModules = true;
            foreach (ModuleSlots moduleSlots in moduleSlotsAll)
            {
                foreach (Module module in moduleSlots.value)
                {
                    if (module != null)
                    {
                        noModules = false;
                    }
                }
            }

            if (noModules)
            {
                PlayerPrefs.SetInt("SpeedUnlocked", 1);
            }
            
        }
        else
        {
            // Setup next floor 
            currentLevel = levels[index];
            spawnChanceStart.value = currentLevel.rewardChanceStart;
            spawnChanceStep.value = currentLevel.rewardChanceStep;

            // Destroy all rooms
            foreach (Transform room in roomsTransform)
            {
                Destroy(room.gameObject);
            }

            // Reset player and camera
            if (!currentLevel.isBossFloor)
            {
                player.transform.position = new Vector3(0, 3.5f, 0);
                Camera mainCamera = Camera.main;
                mainCamera.transform.position = new Vector3(0, mainCamera.transform.position.y, 0);
            }
            else
            {
                player.transform.position = new Vector3(0, 3.5f, -85 * 5);
                Camera mainCamera = Camera.main;
                mainCamera.transform.position = new Vector3(0, mainCamera.transform.position.y, -85 * 5);
            }

            // Generate floor and navmesh
            if (currentLevel.isBossFloor)
            {
                Layout = GenerateBossFloorLayout(currentLevel.bossFloorID);
            }
            else
            {
                Layout = GenerateFloorLayout();
                GenerateFloor(Layout);
            }
            minimapScript.ResetMinimap();
            
            surface.BuildNavMesh();

            index++;
        }

    }

    char[,] GenerateFloorLayout()
    {
        var floor = new char[11, 11];
        floor[5, 5] = 'S';

        int basicRoomsCreated = 0;
        int specialRoomsCreated = 0;

        while (basicRoomsCreated < currentLevel.normalRooms)
        {
            int x = UnityEngine.Random.Range(0, 11);
            int y = UnityEngine.Random.Range(0, 11);

            if (floor[x, y] == '\0')
            {
                if (AdjacentRooms(floor, x, y) > 0)
                {
                    floor[x, y] = '.';
                    basicRoomsCreated++;
                }
            }
        }

        bool transition = false;
        int treasure = currentLevel.moduleRooms;

        while (specialRoomsCreated < currentLevel.challengeRooms + currentLevel.moduleRooms + 1)
        {
            int x = UnityEngine.Random.Range(0, 11);
            int y = UnityEngine.Random.Range(0, 11);

            if (floor[x, y] == '\0')
            {
                if (ValidSpecialRoom(floor, x, y))
                {
                    if (!transition)
                    {
                        floor[x, y] = 'G';
                        transition = true;
                    }
                    else if (treasure != 0)
                    {
                        floor[x, y] = 'T';
                        treasure--;
                    }
                    else
                    {
                        floor[x, y] = 'C';
                    }

                    specialRoomsCreated++;
                }
            }
        }

        return floor;
    }

    char[,] GenerateBossFloorLayout(int index)
    {
        var floor = new char[11, 11];

        List<List<string>> maps = GetBossRooms(index);

        for (int i = 0; i < maps.Count; i++)
        {
            if (i == 0)
            {
                floor[5, i] = 'S';
            }
            else if (i == 1)
            {
                floor[5, i] = 'T';
            } 
            else if (i == maps.Count - 1)
            {
                floor[5, i] = 'G';
            }
            else
            {
                floor[5, i] = 'B';
            }
        }

        for (int i = 0; i < maps.Count; i++)
        {
            bool left = false;
            bool up = true;
            bool right = false;
            bool down = true;

            if (i == 0)
            {
                down = false;
            }
            if (i == maps.Count - 1)
            {
                up = false;
            }
            
            GenerateRoom(new Vector2(0, (i - 5) * (85)), up, right, down, left, maps[i], "Room " + i.ToString(), floor[5, i]);
        }

        return floor;
    }

    int AdjacentRooms(char[,] layout, int x, int y)
    {
        int numberOfAdjecentRooms = 0;
        if (x > 0 && layout[x - 1, y] != '\0') numberOfAdjecentRooms++;
        if (x < 10 && layout[x + 1, y] != '\0') numberOfAdjecentRooms++;
        if (y > 0 && layout[x, y - 1] != '\0') numberOfAdjecentRooms++;
        if (y < 10 && layout[x, y + 1] != '\0') numberOfAdjecentRooms++;
        return numberOfAdjecentRooms;
    }

    bool ValidSpecialRoom(char[,] layout, int x, int y)
    {
        int numberOfAdjecentRooms = 0;
        if (x > 0 && layout[x - 1, y] != '\0') numberOfAdjecentRooms++;
        if (x < 10 && layout[x + 1, y] != '\0') numberOfAdjecentRooms++;
        if (y > 0 && layout[x, y - 1] != '\0') numberOfAdjecentRooms++;
        if (y < 10 && layout[x, y + 1] != '\0') numberOfAdjecentRooms++;

        int numberOfSpecialRooms = numberOfAdjecentRooms;
        if (x > 0 && layout[x - 1, y] != '\0' && layout[x - 1, y] == '.') numberOfSpecialRooms--;
        if (x < 10 && layout[x + 1, y] != '\0' && layout[x + 1, y] == '.') numberOfSpecialRooms--;
        if (y > 0 && layout[x, y - 1] != '\0' && layout[x, y - 1] == '.') numberOfSpecialRooms--;
        if (y < 10 && layout[x, y + 1] != '\0' && layout[x, y + 1] == '.') numberOfSpecialRooms--;

        return (numberOfAdjecentRooms == 1) && (numberOfSpecialRooms == 0);
    }

    JSONNode LoadLevelsFromJSON()
    {
        string path = Application.streamingAssetsPath + "/Levels.json";
        if (File.Exists(path))
        {
            // Read the json from the file into a string
            using (StreamReader stream = new StreamReader(path))
            {
                string json = stream.ReadToEnd();
                return JSON.Parse(json);
            }
        }
        throw new FileNotFoundException(path);
    }

    private List<string> GetRandomRoom(int doors, int floor, string type)
    {
        int count = rooms[type][doors].Count;
        JSONNode maps = rooms[type][doors];
        JSONArray mapJSON = new JSONArray();

        bool isFound = false;

        while (!isFound)
        {
            var val = UnityEngine.Random.Range(0, count);
            if (maps[val]["Floor"].AsInt <= floor)
            {
                isFound = true;
                mapJSON = maps[val]["Map"].AsArray;
            }
        }

        List<string> map = new List<string>();

        foreach (var row in mapJSON)
        {
            map.Add(row.Value);
        }

        return map;
    }

    private List<List<string>> GetBossRooms(int index)
    {
        int count = rooms["Boss"][index].Count;
        JSONNode maps = rooms["Boss"][index];

        List<List<string>> bossMaps = new List<List<string>>();

        for (int i = 0; i < count; i++)
        {
            JSONArray mapJSON = new JSONArray();
            mapJSON = maps[i]["Map"].AsArray;

            List<string> map = new List<string>();

            foreach (var row in mapJSON)
            {
                map.Add(row.Value);
            }

            bossMaps.Add(map);
        }
        
        return bossMaps;
    }

    void GenerateFloor(char[,] layout)
    {
        int rowLength = layout.GetLength(0);
        int colLength = layout.GetLength(1);

        for (int i = 0; i < rowLength; i++)
        {
            for (int j = 0; j < colLength; j++)
            {
                if (layout[i, j] != '\0')
                {
                    bool left = false;
                    bool up = false;
                    bool right = false;
                    bool down = false;

                    if (i > 0 && layout[i - 1, j] != '\0') left = true;
                    if (i < 10 && layout[i + 1, j] != '\0') right = true;
                    if (j > 0 && layout[i, j - 1] != '\0') down = true;
                    if (j < 10 && layout[i, j + 1] != '\0') up = true;
                    
                    if (layout[i, j] == '.')
                    {
                        int doors = (Convert.ToInt32(left) << 3) + (Convert.ToInt32(up) << 2) + (Convert.ToInt32(right) << 1) + Convert.ToInt32(down) - 1;
                        List<string> map = GetRandomRoom(doors, currentLevel.floorNumber, "Normal");
                        GenerateRoom(new Vector2((i - 5) * (125), (j - 5) * (85)), up, right, down, left, map, "Room " + i.ToString() + " " + j.ToString(), '.');
                    }
                    if (layout[i, j] == 'S')
                    {
                        List<string> map = GetRandomRoom(0, currentLevel.floorNumber, "Start");
                        GenerateRoom(new Vector2((i - 5) * (125), (j - 5) * (85)), up, right, down, left, map, "Room " + i.ToString() + " " + j.ToString(), 'S');
                    }
                    if (layout[i, j] == 'G')
                    {
                        int doors = Convert.ToInt32(down) * 3 + Convert.ToInt32(right) * 2 + Convert.ToInt32(up);
                        List<string> map = GetRandomRoom(doors, currentLevel.floorNumber, "Transition");
                        GenerateRoom(new Vector2((i - 5) * (125), (j - 5) * (85)), up, right, down, left, map, "Room " + i.ToString() + " " + j.ToString(), 'G');
                    }
                    if (layout[i, j] == 'T')
                    {
                        int doors = Convert.ToInt32(down) * 3 + Convert.ToInt32(right) * 2 + Convert.ToInt32(up);
                        List<string> map = GetRandomRoom(doors, currentLevel.floorNumber, "Treasure");
                        GenerateRoom(new Vector2((i - 5) * (125), (j - 5) * (85)), up, right, down, left, map, "Room " + i.ToString() + " " + j.ToString(), 'T');
                    }
                    if (layout[i, j] == 'C')
                    {
                        int doors = Convert.ToInt32(down) * 3 + Convert.ToInt32(right) * 2 + Convert.ToInt32(up);
                        List<string> map = GetRandomRoom(doors, currentLevel.floorNumber, "Challenge");
                        GenerateRoom(new Vector2((i - 5) * (125), (j - 5) * (85)), up, right, down, left, map, "Room " + i.ToString() + " " + j.ToString(), 'C');
                    }
                }
            }
        }
    }

    void GenerateRoom(Vector2 center, bool up, bool right, bool down, bool left, List<string> rows, string name, char type = '.')
    {
        var room = new GameObject(name);
        room.transform.parent = roomsTransform;
        Vector2 topLeft = new Vector2(-45, 25);
        GameObject current;

        // Walls
        var walls = new GameObject("Walls");
        walls.transform.parent = room.transform;

        current = Instantiate(wallLongPrefab, new Vector3(-32.5f, 2.5f, 35f), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(60, 10, 15);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallLong;
        current = Instantiate(wallLongPrefab, new Vector3(32.5f, 2.5f, 35f), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(60, 10, 15);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallLong;
        current = Instantiate(wallLongPrefab, new Vector3(-32.5f, 2.5f, -35f), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(60, 10, 15);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallLong;
        current = Instantiate(wallLongPrefab, new Vector3(32.5f, 2.5f, -35f), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(60, 10, 15);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallLong;
        current = Instantiate(wallMediumPrefab, new Vector3(-55, 2.5f, 15), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(15, 10, 25);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallMedium;
        current = Instantiate(wallMediumPrefab, new Vector3(55, 2.5f, 15), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(15, 10, 25);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallMedium;
        current = Instantiate(wallMediumPrefab, new Vector3(-55, 2.5f, -15), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(15, 10, 25);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallMedium;
        current = Instantiate(wallMediumPrefab, new Vector3(55, 2.5f, -15), Quaternion.identity, walls.transform);
        current.transform.localScale = new Vector3(15, 10, 25);
        current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallMedium;

        // Corridors
        var corridors = new GameObject("Corridors");
        corridors.transform.parent = room.transform;
        
        if (left)
        {
            current = Instantiate(corridorPrefab, new Vector3(-55, 0, 0), Quaternion.identity, corridors.transform);
            current.transform.localScale = new Vector3(15, 5, 5);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialGroundHorizontal;
        }
        else
        {
            current = Instantiate(wallPrefab, new Vector3(-55, 2.5f, 0), Quaternion.identity, walls.transform);
            current.transform.localScale = new Vector3(15, 10, 5);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWall;
        }

        if (up)
        {
            current = Instantiate(corridorPrefab, new Vector3(0, 0, 35), Quaternion.identity, corridors.transform);
            current.transform.localScale = new Vector3(5, 5, 15);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialGroundVertical;
        }
        else
        {
            current = Instantiate(wallPrefab, new Vector3(0, 2.5f, 35), Quaternion.identity, walls.transform);
            current.transform.localScale = new Vector3(5, 10, 15);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWall;
        }

        if (right)
        {
            current = Instantiate(corridorPrefab, new Vector3(55, 0, 0), Quaternion.identity, corridors.transform);
            current.transform.localScale = new Vector3(15, 5, 5);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialGroundHorizontal;
        }
        else
        {
            current = Instantiate(wallPrefab, new Vector3(55, 2.5f, 0), Quaternion.identity, walls.transform);
            current.transform.localScale = new Vector3(15, 10, 5);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWall;
        }

        if (down)
        {
            current = Instantiate(corridorPrefab, new Vector3(0, 0, -35), Quaternion.identity, corridors.transform);
            current.transform.localScale = new Vector3(5, 5, 15);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialGroundVertical;
        }
        else
        {
            current = Instantiate(wallPrefab, new Vector3(0, 2.5f, -35), Quaternion.identity, walls.transform);
            current.transform.localScale = new Vector3(5, 10, 15);
            current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWall;
        }

        // Transition lights
        if (type == 'C' || type == 'B')
        {
            if (left)
            {
                current = Instantiate(transitionLightChallengePrefab, new Vector3(-70, 5, 0), Quaternion.identity, corridors.transform);
            }
            if (up)
            {
                current = Instantiate(transitionLightChallengePrefab, new Vector3(0, 5, 50), Quaternion.identity, corridors.transform);
            }
            if (right)
            {
                current = Instantiate(transitionLightChallengePrefab, new Vector3(70, 5, 0), Quaternion.identity, corridors.transform);
            }
            if (down)
            {
                current = Instantiate(transitionLightChallengePrefab, new Vector3(0, 5, -50), Quaternion.identity, corridors.transform);
            }
        }

        if (type == 'G')
        {
            if (left)
            {
                current = Instantiate(transitionLightNextLevelPrefab, new Vector3(-70, 5, 0), Quaternion.identity, corridors.transform);
            }
            if (up)
            {
                current = Instantiate(transitionLightNextLevelPrefab, new Vector3(0, 5, 50), Quaternion.identity, corridors.transform);
            }
            if (right)
            {
                current = Instantiate(transitionLightNextLevelPrefab, new Vector3(70, 5, 0), Quaternion.identity, corridors.transform);
            }
            if (down)
            {
                current = Instantiate(transitionLightNextLevelPrefab, new Vector3(0, 5, -50), Quaternion.identity, corridors.transform);
            }
        }

        if (type == 'T')
        {
            if (left)
            {
                current = Instantiate(transitionLightModulePrefab, new Vector3(-70, 5, 0), Quaternion.identity, corridors.transform);
            }
            if (up)
            {
                current = Instantiate(transitionLightModulePrefab, new Vector3(0, 5, 50), Quaternion.identity, corridors.transform);
            }
            if (right)
            {
                current = Instantiate(transitionLightModulePrefab, new Vector3(70, 5, 0), Quaternion.identity, corridors.transform);
            }
            if (down)
            {
                current = Instantiate(transitionLightModulePrefab, new Vector3(0, 5, -50), Quaternion.identity, corridors.transform);
            }
        }

        // Chasm
        current = Instantiate(currentLevel.chasmPrefab, new Vector3(0, 0, 0), Quaternion.identity, room.transform);

        // RoomController
        current = Instantiate(roomControllerPrefab, new Vector3(0, 0, 0), Quaternion.identity, room.transform);
        var roomController = current.GetComponent<RoomController>();
        roomController.enemies = new List<GameObject>();
        roomController.left = left;
        roomController.up = up;
        roomController.right = right;
        roomController.down = down;
        roomController.rewards = new RewardBucket[] { passiveBigPowerUps, passiveSmallPowerUps, restorativeBigPowerUps, restorativeSmallPowerUps };
        roomController.spawnChance = spawnChance;
        roomController.spawnChanceStart = spawnChanceStart;
        roomController.spawnStep = spawnChanceStep;

        if (type == 'C')
        {
            var collider = roomController.GetComponent<BoxCollider>();
            collider.size = new Vector3(10, 5, 10);
        }

        // Room interior
        var ground = new GameObject("Ground");
        ground.transform.parent = room.transform;

        var enemies = new GameObject("Enemies");
        enemies.transform.parent = room.transform;

        var obstacles = new GameObject("Obstacles");
        obstacles.transform.parent = room.transform;

        var traps = new GameObject("Traps");
        traps.transform.parent = room.transform;
        
        int verticalOffset = 0;

        foreach (var row in rows)
        {
            int horizontalOffset = 0;
            foreach (var character in row)
            {
                if (character == ' ')
                {
                    current = Instantiate(navmeshObstacle, new Vector3(topLeft.x + horizontalOffset, 4, topLeft.y - verticalOffset), Quaternion.identity, ground.transform);
                }

                // Ground
                if (character != ' ')
                {
                    current = Instantiate(groundPrefab, new Vector3(topLeft.x + horizontalOffset, 0, topLeft.y - verticalOffset), Quaternion.identity, ground.transform);
                    current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialGround;
                }

                // Enemies
                if (character == 'T')
                {

                    current = Instantiate(towerPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 3.5f, topLeft.y - verticalOffset), Quaternion.identity, enemies.transform);
                    current.SetActive(false);
                    roomController.enemies.Add(current);
                }
                if (character == 'S')
                {
                    current = Instantiate(shooterPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 4, topLeft.y - verticalOffset), Quaternion.AngleAxis(-90, Vector3.right), enemies.transform);
                    current.SetActive(false);
                    roomController.enemies.Add(current);
                }
                if (character == 'C')
                {
                    current = Instantiate(chaserPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 4, topLeft.y - verticalOffset), Quaternion.identity, enemies.transform);
                    current.SetActive(false);
                    roomController.enemies.Add(current);
                }

                // Traps
                if (character == '2')
                {
                    current = Instantiate(trapShooterPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 3.5f, topLeft.y - verticalOffset), Quaternion.LookRotation(Vector3.right, Vector3.up), traps.transform);
                }
                if (character == '4')
                {
                    current = Instantiate(trapShooterPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 3.5f, topLeft.y - verticalOffset), Quaternion.LookRotation(-Vector3.forward, Vector3.up), traps.transform);
                }
                if (character == '6')
                {
                    current = Instantiate(trapShooterPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 3.5f, topLeft.y - verticalOffset), Quaternion.LookRotation(Vector3.forward, Vector3.up), traps.transform);
                }
                if (character == '8')
                {
                    current = Instantiate(trapShooterPrefab[GetEnemyLevel()], new Vector3(topLeft.x + horizontalOffset, 3.5f, topLeft.y - verticalOffset), Quaternion.LookRotation(-Vector3.right, Vector3.up), traps.transform);
                }

                // Obstacles
                if (character == '#')
                {
                    current = Instantiate(wallInnerPrefab, new Vector3(topLeft.x + horizontalOffset, 5, topLeft.y - verticalOffset), Quaternion.identity, obstacles.transform);
                    current.transform.localScale = new Vector3(5, 5, 5);
                    current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallInner;
                }
                if (character == '$')
                {
                    current = Instantiate(destructibleWallPrefab, new Vector3(topLeft.x + horizontalOffset, 5, topLeft.y - verticalOffset), Quaternion.identity, obstacles.transform);
                    current.transform.localScale = new Vector3(5, 5, 5);
                    current.GetComponent<Renderer>().sharedMaterial = currentLevel.materialWallDestructible;
                }

                // Bonuses
                if (character == 'b')
                {
                    GameObject temp = passiveSmallPowerUps.GetRandomReward();
                    current = Instantiate(temp, new Vector3(topLeft.x + horizontalOffset, 5, topLeft.y - verticalOffset), temp.transform.rotation, room.transform);
                }
                if (character == 'B')
                {
                    GameObject temp = passiveBigPowerUps.GetRandomReward();
                    current = Instantiate(temp, new Vector3(topLeft.x + horizontalOffset, 5, topLeft.y - verticalOffset), temp.transform.rotation, room.transform);
                }

                // Next level transition
                if (character == 'G')
                {
                    current = Instantiate(nextLevelPrefab, new Vector3(topLeft.x + horizontalOffset, 4, topLeft.y - verticalOffset), Quaternion.identity, room.transform);
                    LoadNextLevel script = current.GetComponent<LoadNextLevel>();
                    script.levelGenerator = this;
                }

                // Modules
                if (character == 'M')
                {
                    GameObject module = modules[UnityEngine.Random.Range(0, modules.Length)];
                    current = Instantiate(module, new Vector3(topLeft.x + horizontalOffset, 4, topLeft.y - verticalOffset), module.transform.rotation, room.transform);
                    ModulePowerUp script = current.GetComponent<ModulePowerUp>();
                    script.uIController = uIController;
                }

                horizontalOffset += 5;
            }
            verticalOffset += 5;
        }

        // Room transitions
        var roomTransitions = new GameObject("RoomTransitions");
        roomTransitions.transform.parent = room.transform;

        if (left)
        {
            current = Instantiate(roomTransitionPrefab, new Vector3(-53, 5, 0), Quaternion.identity, roomTransitions.transform);
            var roomTransition = current.GetComponent<RoomTransition>();
            roomTransition.playerMoveVector = new Vector3(-playerTransferDistance, 0, 0);
            roomTransition.cameraMoveVector = new Vector3(-125, 0, 0);
            var collider = current.GetComponent<BoxCollider>();
            collider.size = new Vector3(1, 5, 5);
        }
        if (up)
        {
            current = Instantiate(roomTransitionPrefab, new Vector3(0, 5, 33), Quaternion.identity, roomTransitions.transform);
            var roomTransition = current.GetComponent<RoomTransition>();
            roomTransition.playerMoveVector = new Vector3(0, 0, playerTransferDistance);
            roomTransition.cameraMoveVector = new Vector3(0, 0, 85);
            var collider = current.GetComponent<BoxCollider>();
            collider.size = new Vector3(5, 5, 1);
        }
        if (right)
        {
            current = Instantiate(roomTransitionPrefab, new Vector3(53, 5, 0), Quaternion.identity, roomTransitions.transform);
            var roomTransition = current.GetComponent<RoomTransition>();
            roomTransition.playerMoveVector = new Vector3(playerTransferDistance, 0, 0);
            roomTransition.cameraMoveVector = new Vector3(125, 0, 0);
            var collider = current.GetComponent<BoxCollider>();
            collider.size = new Vector3(1, 5, 5);
        }
        if (down)
        {
            current = Instantiate(roomTransitionPrefab, new Vector3(0, 5, -33), Quaternion.identity, roomTransitions.transform);
            var roomTransition = current.GetComponent<RoomTransition>();
            roomTransition.playerMoveVector = new Vector3(0, 0, -playerTransferDistance);
            roomTransition.cameraMoveVector = new Vector3(0, 0, -85);
            var collider = current.GetComponent<BoxCollider>();
            collider.size = new Vector3(5, 5, 1);
        }

        room.transform.position = new Vector3(center.x, 0, center.y);
    }

    private int GetEnemyLevel()
    {
        int sum = currentLevel.blueSpawnChance + currentLevel.greenSpawnChance + currentLevel.redSpawnChance;

        int chance = UnityEngine.Random.Range(0, sum);
        if (chance < currentLevel.blueSpawnChance)
        {
            return 0;
        }
        else if (chance < currentLevel.blueSpawnChance + currentLevel.greenSpawnChance)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
}
