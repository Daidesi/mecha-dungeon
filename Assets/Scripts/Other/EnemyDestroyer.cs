﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            if (other.transform.parent.tag == "Enemy")
            {
                other.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                other.gameObject.SetActive(false);
            }
        }

    }
}
