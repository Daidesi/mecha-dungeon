﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour {

    public List<GameObject> enemies;
    public GameObject exitWallPrefab;
    public float enemiesSpawnWait = 2f;

    // room rewards
    public FloatVariable spawnChance;
    public FloatVariable spawnStep;
    public FloatVariable spawnChanceStart;
    public RewardBucket[] rewards;

    public bool left = false;
    public bool up = false;
    public bool right = false;
    public bool down = false;

    private List<GameObject> exitWalls;
    private PlayerController playerController;
    private GameObject miniMap;

    private bool isTriggered = false;
    private bool isStarted = false;
    private bool isCompleted = false;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        exitWalls = new List<GameObject>();
        miniMap = GameObject.FindGameObjectWithTag("Minimap");
    }
    
    void Update () {
		if (isStarted && !isCompleted)
        {
            var roomCompleted = true;
            foreach (var enemy in enemies)
            {
                if (enemy.activeSelf)
                {
                    roomCompleted = false;
                    break;
                }
            }
            if (roomCompleted)
            {
                if (enemies.Count != 0)
                {
                    miniMap.SetActive(true);
                    playerController.OnRoomCompletion();
                    SpawnReward();
                }

                isCompleted = true;
                foreach (var exitWall in exitWalls)
                {
                    exitWall.SetActive(false);
                }
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (!isTriggered && other.tag == "PlayerComponent")
        {
            isTriggered = true;
            if (enemies.Count != 0)
            {
                if (miniMap != null)
                {
                    miniMap.SetActive(false);
                }
                playerController.OnRoomEnter();
                CreateEnemySpawnEffects();
                StartCoroutine(SpawnEnemies());
                SpawnExitWalls();
            }
        }
    }

    private void CreateEnemySpawnEffects()
    {
        foreach (var enemy in enemies)
        {
            var effect = enemy.GetComponent<EnemyController>().spawnEffectAnimation;
            var effectObject = Instantiate(effect, enemy.transform.position, Quaternion.identity, transform);
            Destroy(effectObject, 3f);
        }
    }

    private IEnumerator SpawnEnemies()
    {
        yield return new WaitForSeconds(enemiesSpawnWait);

        foreach (var enemy in enemies)
        {
            enemy.SetActive(true);
        }
        isStarted = true;
    }

    private void SpawnExitWalls()
    {
        GameObject current;
        if (left)
        {
            current = Instantiate(exitWallPrefab, transform);
            current.transform.localPosition = new Vector3(-48, 5, 0);
            current.transform.localScale = new Vector3(1, 5, 5);
            exitWalls.Add(current);
        }
        if (up)
        {
            current = Instantiate(exitWallPrefab, transform);
            current.transform.localPosition = new Vector3(0, 5, 28);
            current.transform.localScale = new Vector3(5, 5, 1);
            exitWalls.Add(current);
        }
        if (right)
        {
            current = Instantiate(exitWallPrefab, transform);
            current.transform.localPosition = new Vector3(48, 5, 0);
            current.transform.localScale = new Vector3(1, 5, 5);
            exitWalls.Add(current);
        }
        if (down)
        {
            current = Instantiate(exitWallPrefab, transform);
            current.transform.localPosition = new Vector3(0, 5, -28);
            current.transform.localScale = new Vector3(5, 5, 1);
            exitWalls.Add(current);
        }
    }

    private void SpawnReward()
    {
        float chance = Random.Range(0f, 1f); 
        if (chance < spawnChance.value)
        {
            float x, y;
            FindEmptyBlock(out x, out y);
            GameObject rewardPrefab = GetReward();
            GameObject reward = Instantiate(rewardPrefab, transform, false);
            reward.transform.localPosition = new Vector3(x, 4, y);

            GameObject effect = Instantiate(reward.GetComponent<BasicPowerUp>().powerUpTakenEffectPrefab, reward.transform.position, Quaternion.identity);
            Destroy(effect, 3);

            spawnChance.value = spawnChanceStart.value;
        }
        spawnChance.value += spawnStep.value;
    }

    private void FindEmptyBlock(out float x, out float y)
    {
        x = 0;
        y = 0;

        int distance = 0;

        Collider[] hitObjects = Physics.OverlapBox(transform.position + new Vector3(x, 3, y), new Vector3(2.4f, 2.4f, 2.4f));
        if (hitObjects.Length == 2 && hitObjects[0].tag == "Ground" && hitObjects[1].tag == "Ground")
        {
            return;
        }

        while (true)
        {
            distance++;
            x -= 5;
            for (int i = 0; i < distance; i++)
            {
                hitObjects = Physics.OverlapBox(transform.position + new Vector3(x, 3, y), new Vector3(2.4f, 2.4f, 2.4f));
                if (hitObjects.Length == 2 && hitObjects[0].tag == "Ground" && hitObjects[1].tag == "Ground")
                {
                    return;
                }
                x += 5;
                y -= 5;
            }
            for (int i = 0; i < distance; i++)
            {
                hitObjects = Physics.OverlapBox(transform.position + new Vector3(x, 3, y), new Vector3(2.4f, 2.4f, 2.4f));
                if (hitObjects.Length == 2 && hitObjects[0].tag == "Ground" && hitObjects[1].tag == "Ground")
                {
                    return;
                }
                x += 5;
                y += 5;
            }
            for (int i = 0; i < distance; i++)
            {
                hitObjects = Physics.OverlapBox(transform.position + new Vector3(x, 3, y), new Vector3(2.4f, 2.4f, 2.4f));
                if (hitObjects.Length == 2 && hitObjects[0].tag == "Ground" && hitObjects[1].tag == "Ground")
                {
                    return;
                }
                x -= 5;
                y += 5;
            }
            for (int i = 0; i < distance; i++)
            {
                hitObjects = Physics.OverlapBox(transform.position + new Vector3(x, 3, y), new Vector3(2.4f, 2.4f, 2.4f));
                if (hitObjects.Length == 2 && hitObjects[0].tag == "Ground" && hitObjects[1].tag == "Ground")
                {
                    return;
                }
                x -= 5;
                y -= 5;
            }

        }
    }
    
    private GameObject GetReward()
    {
        int sum = 0;
        for (int i = 0; i < rewards.Length; i++)
        {
            sum += rewards[i].basicSpawnChance + rewards[i].bonusSpawnChance;
        }

        int chance = Random.Range(0, sum);
        for (int i = 0; i < rewards.Length; i++)
        {
            if (chance < (rewards[i].basicSpawnChance + rewards[i].bonusSpawnChance))
            {
                return rewards[i].GetRandomReward();
            }
            else
            {
                chance -= rewards[i].basicSpawnChance + rewards[i].bonusSpawnChance;
            }
        }
        // should never get here
        return null;
    }
}
