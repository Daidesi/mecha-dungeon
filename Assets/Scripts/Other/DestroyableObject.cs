﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour, IDealableDamage
{
    public float maxHealth = 50f;
    public GameObject destroyedEffectPrefab;
    public float minOffset = 0.2f;

    private float health = 50f;
    private Renderer _renderer;

    private void Start()
    {
        health = maxHealth;
        _renderer = GetComponent<Renderer>();
        Material mat = _renderer.sharedMaterial;
        _renderer.sharedMaterial = Instantiate(mat);
    }

    public void DealDamage(float damageAmount, GameObject source)
    {
        health -= damageAmount;
        _renderer.sharedMaterial.SetFloat("_SecondTextureIntensity", Mathf.Clamp(1 - (health / maxHealth) + minOffset, minOffset, 1));

        if (health <= 0)
        {
            Kill();
        }
    }

    private void Kill()
    {
        gameObject.SetActive(false);
        GameObject effect = Instantiate(destroyedEffectPrefab, transform.position, Quaternion.identity);
        Destroy(effect, 3);
    }
}
