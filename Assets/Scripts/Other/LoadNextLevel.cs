﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevel : MonoBehaviour {

    public GenerateLevel levelGenerator;

    private bool isTriggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!isTriggered && other.tag == "PlayerComponent")
        {
            isTriggered = true;
            levelGenerator.GenerateNextFloor();
        }
    }
}
