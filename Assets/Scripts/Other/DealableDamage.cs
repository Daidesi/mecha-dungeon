﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDealableDamage{

    void DealDamage(float damageAmount, GameObject source);
}
