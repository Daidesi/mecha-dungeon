﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAppliableDoT {

    void ApplyDoTEffect(float damagePerSec, float duration);
}
